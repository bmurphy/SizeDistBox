%First  Total Mass (need to fix the output here)
[Num, gmd, stdev] = get_modal_from_sect(sdResult);

mass_gain = tout .* condrate;
fprintf(1,'\nFinal Conditions. Elapsed Time: %4.1f Mins. Mass Gain = %4.2f ug m-3\n',tout/60, mass_gain )
fprintf(1,'Number [N m-3]: \t%7.3e\n', Num(end))
fprintf(1,'Diameter [m]: \t\t%7.3e\n', gmd(end))
fprintf(1,'Stnd Dev :    \t\t%7.3e\n', stdev(end))
fprintf(1,'Surface Area [m2 m-3]: \t%7.3e\n', sum(sdResult.SA_sect(end,:)) )
fprintf(1,'Mass [ug m-3]: \t\t%7.3e\n', sum(sdResult.Mass_sect(end,:)))

%Make a bunch of Size Distribution plots
set_plot_props

figTitle = ['ParticleBox_' distName '_' mergeType];

%%%% Loop through time and plot the size distribution %%%%
pi = 3.1415;
tvec = 0:dt:tout;
nt   = length(tvec);
Dlim = sd.Dplot;  %Plotting Limits for X-axis
Nlim = [1e-5,3e10]; %[N/m3/m
Slim = [1e-20,2e-3];  %[m2/m3/m]
Mlim = [1e-10,1500]; %[ug/m-3/m]
scolor = ['k','g','c','m'];

plot_times = floor(linspace(1,nt,5));
% plot_times = [[1:2:10], floor(linspace(11,nt,10))];
% plot_times = [1:length(tvec)];
% plot_times = [80, 90, 100, 110, 112];

for it = plot_times(end:-1:1)

    F1 = makeFig(figTitle, 'skinny_page','landscape');
    
    %Get Size Parameters
    xdg  = sdResult.dp(it,:); %Diameter axis (m)
    
    %Convert Size Distribution Parameters to a Number Trend (dNdlog10Dp)
    dNdlog10dp = get_ndist_sect(xdg, sdResult, it);
    
    %Convert Number Distribution to Surface Area and Plot
    dSdlog10dp = pi .* xdg.^2 .* dNdlog10dp;
        
    %Convert Number Distribution to Mass and Plot
    dMdlog10dp = sd.ro .* pi./6 .* xdg.^3 .* dNdlog10dp;

    %Plot Number Distribution
    P1 = subplot(1,3,1);
    plot( log10( xdg ), dNdlog10dp, 'color','b','marker','none','linestyle','-','linewidth',2)
    hold on
    bounds = get(gca, 'OuterPosition');
    set(gca, 'OuterPosition', [bounds(1:3) .95])
    
    xlabel('log_{10} [Diameter (m)]')
    ylabel('dNdlog10dp')
    ylim(Nlim)
    xlim(Dlim)
    set(gca,'yscale','log')
    
    
    %Plot Surface Distribution
    P2 = subplot(1,3,2);
    plot( log10( xdg ), dSdlog10dp, 'color','b','marker','none','linestyle','-','linewidth',2)
    hold on
    bounds = get(gca, 'OuterPosition');
    set(gca, 'OuterPosition', [bounds(1:3) .95])
    
    xlabel('log_{10} [Diameter (m)]')
    ylabel('dSdlog10dp')
    ylim(Slim)
    xlim(Dlim)
    set(gca,'yscale','log')

    %Plot Mass Distribution
    P3 = subplot(1,3,3);
    plot( log10( xdg ), dMdlog10dp, 'color','b','marker','none','linestyle','-','linewidth',2)
    hold on
    bounds = get(gca, 'OuterPosition');
    set(gca, 'OuterPosition', [bounds(1:3) .95])
    
    xlabel('log_{10} [Diameter (m)]')
    ylabel('dMdlog10dp')
    ylim(Mlim)
    xlim(Dlim)
    set(gca,'yscale','log')

    subtitle(['(TOMAS) Particle Size Distributions after ' num2str(tvec(it)) ' sec'],'fontsize',14);
       
end


%%%% Make Plots that look like Whitby et al. 2002
F2 = makeFig(figTitle, 'half_page','landscape');

%Plot Number Distribution
P1 = subplot(2,2,1);
plot( tvec, Num ./1.e6, 'marker','none','linestyle','-','linewidth',2)
hold on
bounds = get(gca, 'OuterPosition');
% set(gca, 'OuterPosition', [bounds(1:3) .95])
    
xlabel('Time [s]')
ylabel('Number Concentration [#/cc]')
ylim([0.0, 1.4e4])
xlim([tvec(1), tvec(end)])
set(gca,'yscale','linear')

%Plot Mean Size
P2 = subplot(2,2,2);
plot( tvec, gmd, 'marker','none','linestyle','-','linewidth',2)
hold on
bounds = get(gca, 'OuterPosition');
% set(gca, 'OuterPosition', [bounds(1:3) .95])    

xlabel('Time [s]')
ylabel('Mean Size [m]')
ylim([1.0e-9, 1.0e-3])
xlim([tvec(1), tvec(end)])
set(gca,'yscale','log')


%Plot Standard Deviation
P3 = subplot(2,2,3);
plot( tvec, stdev, 'marker','none','linestyle','-','linewidth',2)
hold on
bounds = get(gca, 'OuterPosition');
% set(gca, 'OuterPosition', [bounds(1:3) .95])

xlabel('Time [s]')
ylabel('Standard Deviation [-]')
ylim([1,3])
xlim([tvec(1), tvec(end)])
set(gca,'yscale','linear')


%Plot Total Mass Change
P4 = subplot(2,2,4);
plot( tvec, sum(sdResult.Mass_sect,2), 'marker','none','linestyle','-','linewidth',2)
hold on
bounds = get(gca, 'OuterPosition');
% set(gca, 'OuterPosition', [bounds(1:3) .95])

xlabel('Time [s]')
ylabel('Mass [ug m-3]')
ylim([0,1000])
xlim([tvec(1), tvec(end)])
set(gca,'yscale','linear')


subtitle(['TOMAS Modal Properties After ' num2str(tvec(end)) ' sec'],'fontsize',14);

    


