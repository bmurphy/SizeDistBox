function sd = movingSect( sd, vap, condrate, dt, M3_Grow_Method )

%This function solves the conde/evap equations using a moving sectional
%technique adapted from code for thermodenuder experiments originally
%writeen by Riipinen et al. The Kelvin effect can be included in this one.
%Particles do not move from size bin to size bin. In theory, each bin can
%respond independently to the vapor pressure corresponding to the
%composition in that bin. In practice, this is not consistent with the
%other algorithms being tested here so one condensation or evaporation rate
%is applied and distributed to all the size bins, similary to how it is
%done for the Modal and TOMAS approaches. An ODE solver is used to advance
%the distirbution in time.

%Constants
R  = 8.314472;  %Molar gas constant (J/mol/K)
T = 298;           % K
k = 1.3806503e-23; % Boltzmann constant [J/K]
Na = 6.0221415e23; % Avogadro's number [1/mol]
p = 101325.0; % Total pressure [Pa]
pi = 3.1415;
MW = vap.MW; %kg mol-1
Dn = vap.dv; %m2 s-1
cbar = vap.cbar; %m s-1
rho = sd.ro .* 1.0e-9; %kg m-3
sigma1 = 0.05; % Surface tensions of the surrogate compounds [N/m]
alpha = 1.0; %Accomodation Coefficient

% Set up Size Distribution
nbins = length(sd.Num_sect);
Dp = sd.dp;        % m
Nk = sd.Num_sect;  % N m-3
Mk = sd.Mass_sect .* 1.0e-9; % kg m-3

% Set Up Amount of Vapor to Condense (assume nonvolatile vapor)
switch M3_Grow_Method
    case 'Conc'
        %Vapor Concentration is passed directly
        Gc   = vap.conc .* 1.0e-9; %kg m-3
        csat = vap.Cstar .* 1.0e-9; %kg m-3
    otherwise
        %Vapor concentration is inferred from condensation rate
        Gc   = condrate .* dt .* 1.0e-9; % kg m-3
        csat = vap.Cstar .* 1.0e-9; %kg m-3
end

% Set Input Data
input = zeros(1,nbins+1); 
input(1:nbins) = Mk; % Masses of each species in each particle (kg m-3)
input(nbins+1) = Gc; % Gas phase concentrations (kg m-3)

% Solving the mass fluxes 
time = linspace(0,dt);
options = odeset('RelTol',1E-10,'AbsTol',1E-19);    
[tout, output0] = ode45(@fluxes_size_dist, time, input,  options, dt,nbins,...
    Nk,MW,sigma1,rho,Dn,p,T,alpha,cbar,csat);

% Temporary evolution of particle and gas phase masses (kg) and (kg/m3)
Mk_out(1:length(time),1:nbins) = output0(1:length(time),1:nbins);
Gc_out(1:length(time)) = output0(1:length(time),nbins+1);

Mk_out( Mk_out <= 0.0 ) = 0.0; 
Gc_out( Gc_out <= 0.0 ) = 0.0; 

% Particle and gas phase masses at the end of the heating section (kg) and
% (kg/m3)
Mk_f(1:nbins) = Mk_out(end,1:nbins);
Gc_f = Gc_out(end,1);

% Particle diameters
Dp_end = ((3.*(Mk_f./Nk)./4./pi./rho).^(1./3)) .* 2.0; %m
Dp_end( isnan(Dp_end) | Dp_end == 0.0 ) = 1.0e-9;

%Sometimes the Diameters get out of order. Reorder them if necessary
[tmp, isort] = sort( Dp_end );
Dp_end = Dp_end( isort );
Mk_f = Mk_f( isort );
Nk = Nk( isort );

%Pass Vairables Back to Upper-level Routine
sd.Num_sect  = Nk; %N m-3
sd.Mass_sect = max( Mk_f .* 1.0e9, 1.0e-20 ); %ug m-3
sd.dp = Dp_end;

end


%%%%%%%%%%%%%%%%%%%%%%%%% Flux Calculation %%%%%%%%%%%%%%%%%%%%%%%%%%%
function flx = fluxes_size_dist(t,input0,dt,nbins,...
    Nk,MW,sigma1,rho,Dn,press,T,alpha,cbar,csat)
    
R = 8.314472;

% Mass in each size bin (kg m-3)
Pc(1:nbins) = input0(1:nbins);
% Gas phase mass concentration (kg m-3)
Gc = input0(nbins+1);

%Pc( Pc<=0.0 | imag(Pc)~=0.0 ) = 0.0;
if Gc <= 0.0 || imag(Gc)~=0.0; Gc = 0.0; end

% Calculating the composition
Vp = zeros(1,nbins);
Rp = zeros(1,nbins);
peq = zeros(1,nbins);
beeta = zeros(1,nbins);

%Calculating Vapor Pressure [pa]
psat = csat .*R.*T./MW; %Convert saturation concentrations to vapor pressure

for j = 1:nbins  
    if Pc(j) > 1.0e-22;
        % Particle volume and size
        Vp(j) = Pc(j)./Nk(j)./rho;
        Rp(j) = (3.*Vp(j)./4./pi).^(1./3);
        % Kelvin effect
        Ke(j) = exp(2.0 .* MW .* sigma1 ./ R./T ./ rho ./ Rp(j) );
        % Mass based! Remember to fix back!!
        peq(j) = psat.*Ke(j);
        
        % Transitional correction
        % Mean free path of the gas molecules:
        lambda = 3.*Dn./cbar;
        % Knudsen number
        Kn(j) = lambda ./ Rp(j);
        % Fuchs and Sutugin transition regime correction
        beeta(j) = (1.0 + Kn(j))./ ...
            (1.0 + (4./alpha./3 + 0.377).*Kn(j) + 4.*Kn(j).^2./alpha./3);
    end
end

pv_a = peq; % Pressure at the particle surface
pv_i = Gc .*R.*T./MW; % Partial pressures far away from the particles
flx = zeros( 1,nbins );

for j = 1:nbins
    if Pc(j) > 1.0e-19 & (1.0 - pv_a(j)./press)/(1.0 - pv_i./press) > 0.0
        % Mass flux of each compound to the particles
        flx(j) = 4.*pi.*Rp(j).*press.*Dn.*beeta(j).*MW.*log((1.0 - pv_a(j)./press)./(1.0 - pv_i./press))./R./T;
    elseif Pc > 1.0e-19 & (1.0 - pv_a(j)./press)/(1.0 - pv_i./press) <= 0.0
        flx(j) = 4.*pi.*Rp(j)*Dn.*beeta(j).*MW.*(pv_i-pv_a(j))./R./T;
    else
        flx(j) = 0.0;
    end
    flx(j) = Nk(j).*flx(j);  % kg m-3
end

% Mass flux of each compound to the gas phase
flx(nbins+1) = -sum(flx(1:end-1)); % kg m-3

% Changing to column vector
flx = flx';
flx( isnan(flx) | imag(flx)~=0.0 ) = 0.0;

end