function [sd, Dcut] = MMA( sd, GM3 )

%Mode Merging Algorithm from CMAQ
%Coded in MATLAB by Ben Murphy September 2015

Dcut = zeros( 2,sd.nmode,3 ) .* NaN;
MMA.tomode = zeros( 1,sd.nmode );
MMA.Num = zeros( 1,sd.nmode );
MMA.M2 = zeros( 1,sd.nmode );
MMA.M3 = zeros( 1,sd.nmode );


for imode = 1:sd.nmode-1
    
    %%%%%%%%% GROWING MODES    %%%%%%%%%%
    %Find the next highest mode
%     lgrow = 0;  %Mode to grow into
%     dgrow = 1e-3; %Diameter of next highest mode
%     for jmode = 1:sd.nmode
%         if (imode ~= jmode && sd.dg(jmode) > sd.dg(imode) && sd.dg(jmode) < dgrow)
%             lgrow = jmode;
%             dgrow = sd.dg(jmode);
%         end
%     end
    lgrow = imode + 1;
    
    %Check for all of the conditions that are implemented in CMAQ
    if (lgrow && GM3(imode) > GM3(lgrow) && sd.Num(imode) > sd.Num(lgrow) )
        
        [AAA, Dij] = getaf( sd, imode, lgrow );  %Calculate the overlap point
        %Save the cutoff diameter for this mode transition
        Dcut( 1,imode,1 ) = Dij;
        
        if (isreal(AAA) && AAA > 0)
            %Ensure that no more than half of the mode is merged during a time
            %step.
            xxm3 = 3.0 .* log(sd.sg(imode)) ./ sqrt(2);
            xnum = max( AAA, xxm3 );
            Dcut( 1,imode,1 ) = exp( xnum .* sqrt(2) .* log(sd.sg(imode)) ) .* sd.dg(imode); %[m]
            
            xxm2 = 2/3 * xxm3;  %Second Mode 
            xm2 = xnum - xxm2;  %Set up for 2nd moment transfer
            xm3 = xnum - xxm3;  %Set up for 3rd moment transfer
            Dcut( 1,imode,2 ) = exp( xxm2 .* sqrt(2) .* log(sd.sg(imode)) ) .* sd.dg(imode); %[m]
            Dcut( 1,imode,3 ) = exp( xxm3 .* sqrt(2) .* log(sd.sg(imode)) ) .* sd.dg(imode); %[m]
        
            %Calculate the fraction of each moment with diameter greater than
            %the intersection diameter
            FNUM = 0.5 .* erfc( xnum );  %Eqn 10a of Binkowski and Roselle, 2003
            FM2  = 0.5 .* erfc( xm2 );   %Eqn 10b
            FM3  = 0.5 .* erfc( xm3 );
        
            %Calculate the fraction of each moment with diameter less than the
            %cutoff diameter
            PHNUM = 0.5 .* (1.0 + erf( xnum ));  %Eqn 10c
            PHM2  = 0.5 .* (1.0 + erf( xm2 ));   %Eqn 10d
            PHM3  = 0.5 .* (1.0 + erf( xm3 ));   %Eqn 10d
        
            MMA.tomode(imode) = lgrow;
            MMA.Num(imode)    = sd.Num(imode) .* FNUM;
            MMA.M2(imode)     = sd.M2(imode) .* FM2;
            MMA.M3(imode)     = sd.M3(imode) .* FM3;
            
            
        end
        
    end
        
        
    
    %%%%%%%%  SHRINKING MODES   %%%%%%%%%
    %Find the next lowest mode
    lshrink = 0;  %Mode to grow into
    dshrink = 1e-10; %Diameter of next highest mode
    for jmode = 1:sd.nmode
        if (imode ~= jmode && sd.dg(jmode) < sd.dg(imode) && sd.dg(jmode) > dshrink)
            lshrink = jmode;
            dshrink = sd.dg(jmode);
        end
    end
    
    lshrink = 0;
    
    if (lshrink)
        
        [AAA, Dij] = getaf( sd, lshrink, imode ); %Calculate the overlap point
        Dcut( 2,imode ) = Dij;

        %Ensure that no more than half of the mode is merged during a time
        %step.
        xxm3 = 2.0 .* log(sd.sg(imode)) ./ sqrt(2);
        xnum = min( AAA, xxm3 );
        
        xxm2 = 2/3 * xxm3;  %Second Mode 
        xm2 = xnum - xxm2;  %Set up for 2nd moment transfer
        xm3 = xnum - xxm3;  %Set up for 3rd moment transfer
        
        %Calculate the fraction of each moment with diameter greater than
        %the intersection diameter
        FNUM = 0.5 .* erfc( xnum );  %Eqn 10a of Binkowski and Roselle, 2003
        FM2  = 0.5 .* erfc( xm2 );   %Eqn 10b
        FM3  = 0.5 .* erfc( xm3 );
        
        %Calculate the fraction of each moment with diameter less than the
        %cutoff diameter
        PHNUM = 0.5 .* (1.0 + erf( xnum ));  %Eqn 10c
        PHM2  = 0.5 .* (1.0 + erf( xm2 ));   %Eqn 10d
        PHM3  = 0.5 .* (1.0 + erf( xm3 ));   %Eqn 10d
        
        %Update Receptor Mode Concentrations
        sd.Num(lshrink) = sd.Num(lshrink) + sd.Num(imode) .* FNUM;
        sd.M2(lshrink)  = sd.M2(lshrink)  + sd.M2(imode)  .* FM2;
        sd.M3(lshrink)  = sd.M3(lshrink)  + sd.M3(imode)  .* FM3;
        
        %Update Donor Mode Concentrations
        sd.Num(imode) = sd.Num(imode) .* PHNUM;
        sd.M2(imode)  = sd.M2(imode)  .* PHM2;
        sd.M3(imode)  = sd.M3(imode)  .* PHM3;
        
    end

end

%Update All Modes with Merging Changes
for imode = 1:sd.nmode
    if MMA.tomode(imode)
        lgrow = MMA.tomode(imode);
    
        %Update Receptor Mode Concentrations
        sd.Num(lgrow) = sd.Num(lgrow) + MMA.Num(imode);
        sd.M2(lgrow)  = sd.M2(lgrow)  + MMA.M2(imode);
        sd.M3(lgrow)  = sd.M3(lgrow)  + MMA.M3(imode);
        
        %Update Donor Mode Concentrations
        sd.Num(imode) = sd.Num(imode) - MMA.Num(imode);
        sd.M2(imode)  = sd.M2(imode)  - MMA.M2(imode);
        sd.M3(imode)  = sd.M3(imode)  - MMA.M3(imode);

    end


end