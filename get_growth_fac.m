function [Ifac, omega] = get_growth_fac( sd, vap )

%This function calculates the size-dependent growth rates for each mode.
%Its based on papers by Whitby et al. 1991, and Binkowski and Shankar 1995
%
% Ben Murphy September 2015
%
%  sd - size distribution properties
%  dv - molecular diffusivity of condensing vapor
%  cbar - kinetic velocity of condensing vapor
%  Ifac - size-dependent term in condensation rate
%  omega - fractional mass transfer to/from each mode

pi = 3.1415;
alpha = 1.0; %accomodation coefficient
Ifac  = zeros( 2,sd.nmode );
omega = zeros( 2,sd.nmode );

for imode = 1:sd.nmode

    %Equation A15 of Binkowski and Shankar (1995) for the second and third
    %moments of a lognormal distribution of arbitrary size
    M1 = sd.Num(imode) .* sd.dg(imode) .* exp( 0.5 .* (log(sd.sg(imode))).^2 );
        
    GNC2 = 2.*pi .* vap.dv .* sd.Num(imode);
    GNC3 = 2.*pi .* vap.dv .* M1;
    GFM2 = pi./4 .* alpha .* vap.cbar .* M1;
    GFM3 = pi./4 .* alpha .* vap.cbar .* sd.M2(imode);
    
    %Implement eqn A13
    Ifac( 1,imode ) = GNC2 .* GFM2 ./ (GNC2 + GFM2 );
    Ifac( 2,imode ) = GNC3 .* GFM3 ./ (GNC3 + GFM3 );
    
end

%Calculate fractional growth to each mode normalize by total growth
omega(1,:) = Ifac(1,:) ./ sum( Ifac(1,:) );  %Second Moment Growth Fraction
omega(2,:) = Ifac(2,:) ./ sum( Ifac(2,:) );  %Third Moment Growth Fraction


end


%%% Function for Calculating the Condensation Growth Rate during this step
function [Ifac, omega] = get_growth_fac3( sd, vap )

%This function calculates the size-dependent growth rates for each mode.
%Its based on papers by Whitby et al. 1991, and Binkowski and Shankar 1995
%
% Ben Murphy September 2015
%
%  sd - size distribution properties
%  dv - molecular diffusivity of condensing vapor
%  cbar - kinetic velocity of condensing vapor
%  Ifac - size-dependent term in condensation rate
%  omega - fractional mass transfer to/from each mode

pi = 3.1415;
alpha = 1.0; %accomodation coefficient
Ifac  = zeros( 3,sd.nmode );
omega = zeros( 3,sd.nmode );

for imode = 1:sd.nmode

    %Equation A15 of Binkowski and Shankar (1995) for the second and third
    %moments of a lognormal distribution of arbitrary size
    M1 = sd.Num(imode) .* sd.dg(imode) .* exp( 0.5 .* (log(sd.sg(imode))).^2 );
    Mn1 = sd.Num(imode) ./ sd.dg(imode) .* exp( 0.5 .* (log(sd.sg(imode))).^2 );
    
    GNC1 = 2.*pi .* vap.dv; % .* Mn1;
    GNC2 = 2.*pi .* vap.dv .* sd.Num(imode);
    GNC3 = 2.*pi .* vap.dv .* M1;
    
    GFM1 = pi./4 .* alpha .* vap.cbar .* sd.Num(imode);
    GFM2 = pi./4 .* alpha .* vap.cbar .* M1;
    GFM3 = pi./4 .* alpha .* vap.cbar .* sd.M2(imode);
    
    %Implement eqn A13
    Ifac( 1,imode ) = GNC1 .* GFM1 ./ (GNC1 + GFM1 );
    Ifac( 2,imode ) = GNC2 .* GFM2 ./ (GNC2 + GFM2 );
    Ifac( 3,imode ) = GNC3 .* GFM3 ./ (GNC3 + GFM3 );
    
end

%Set Growth Factors to 0 if the mode is Deactivated
Ifac = Ifac .* repmat(sd.Active,3,1);

%Calculate fractional growth to each mode normalize by total growth
omega(1,:) = Ifac(1,:) ./ sum( Ifac(1,:) );  %First Moment Growth Fraction
omega(2,:) = Ifac(2,:) ./ sum( Ifac(2,:) );  %Second Moment Growth Fraction
omega(3,:) = Ifac(3,:) ./ sum( Ifac(3,:) );  %Third Moment Growth Fraction

end


%%% Function for Calculating the Condensation Growth Rate during this step
function [Ifac, omega] = get_growth_fac4( sd, vap, met )

%This function calculates the size-dependent growth rates for each mode.
%Its based on papers by Whitby et al. 1991, and Binkowski and Shankar 1995
%
% Ben Murphy September 2015
%
%  sd - size distribution properties
%  dv - molecular diffusivity of condensing vapor
%  cbar - kinetic velocity of condensing vapor
%  Ifac - size-dependent term in condensation rate
%  omega - fractional mass transfer to/from each mode

pi = 3.1415;
k_B = 1.3806e-23; %Boltzmann Constant
AMU = 1.458E-6 .* met.T .* met.T .^2 ./ ( met.T + 110.4 ); %Dynamic Viscosity kg m-1 s-1
XLM = 6.6328E-8 .* 101325 .* met.T ./ ( 298 .* met.P ); %Mean Free Path of Air [m]
alpha = 1.0; %accomodation coefficient
Ifac  = zeros( 3,sd.nmode );
omega = zeros( 3,sd.nmode );

for imode = 1:sd.nmode

    %Diffusivity and Speed of Particles
    cbar_ptcl = ( 8.0 .* met.T .* k_B ./ ( 3.1415 .* sd.Mass( imode ) ) ) .^ 0.5;
    Kn = XLM ./ sd.dg( imode );
    Cc = 1 + Kn .* ( 1.257 + 0.4 .* exp( -1.1 ./ Kn ) );
    diff_ptcl = k_B .* met.T .* Cc ./ ( 3.0 .* pi .* AMU .* sd.dg( imode ) ); %m2 s-1
    
    
    %Equation A15 of Binkowski and Shankar (1995) for the second and third
    %moments of a lognormal distribution of arbitrary size
    M1 = sd.Num(imode) .* sd.dg(imode) .* exp( 0.5 .* (log(sd.sg(imode))).^2 );
    
    GNC1 = 2.*pi .* ( vap.dv + diff_ptcl );
    GNC2 = 2.*pi .* ( vap.dv + diff_ptcl ) .* sd.Num(imode);
    GNC3 = 2.*pi .* ( vap.dv + diff_ptcl ) .* ( M1 + sd.Num(imode) .* vap.diam );
    
    GFM1 = alpha .* pi./4 .* sqrt( vap.cbar.^2 + cbar_ptcl.^2 ) ...
           .* ( sd.Num(imode) );
    GFM2 = alpha .* pi./4 .* sqrt( vap.cbar.^2 + cbar_ptcl.^2 ) ...
           .* ( M1 + 2.0 .* sd.Num(imode) .* vap.diam );
    GFM3 = alpha .* pi./4 .* sqrt( vap.cbar.^2 + cbar_ptcl.^2 ) ...
           .* ( sd.M2(imode) + 2.0 .* M1 .* vap.diam + sd.Num(imode) .* vap.diam .^ 2 );
    
    %Implement eqn A13
    Ifac( 1,imode ) = GNC1 .* GFM1 ./ (GNC1 + GFM1 );
    Ifac( 2,imode ) = GNC2 .* GFM2 ./ (GNC2 + GFM2 );
    Ifac( 3,imode ) = GNC3 .* GFM3 ./ (GNC3 + GFM3 );
    
end

%Set Growth Factors to 0 if the mode is Deactivated
Ifac = Ifac .* repmat(sd.Active,3,1);

%Calculate fractional growth to each mode normalize by total growth
omega(1,:) = Ifac(1,:) ./ sum( Ifac(1,:) );  %First Moment Growth Fraction
omega(2,:) = Ifac(2,:) ./ sum( Ifac(2,:) );  %Second Moment Growth Fraction
omega(3,:) = Ifac(3,:) ./ sum( Ifac(3,:) );  %Third Moment Growth Fraction

end


%%% Function for Calculating the Condensation Growth Rate during this step
function [Ifac, omega] = get_growth_fac6( sd, vap, met )

%This function calculates the size-dependent growth rates for each mode.
%Its based on papers by Whitby et al. 1991, and Binkowski and Shankar 1995
%
% Ben Murphy September 2015
%
%  sd - size distribution properties
%  dv - molecular diffusivity of condensing vapor
%  cbar - kinetic velocity of condensing vapor
%  Ifac - size-dependent term in condensation rate
%  omega - fractional mass transfer to/from each mode

pi = 3.1415;
k_B = 1.3806e-23; %Boltzmann Constant
AMU = 1.458E-6 .* met.T .* met.T .^2 ./ ( met.T + 110.4 ); %Dynamic Viscosity kg m-1 s-1
XLM = 6.6328E-8 .* 101325 .* met.T ./ ( 298 .* met.P ); %Mean Free Path of Air [m]
alpha = 1.0; %accomodation coefficient
Ifac  = zeros( 3,sd.nmode );
omega = zeros( 3,sd.nmode );

for imode = 1:sd.nmode

    %Diffusivity and Speed of Particles
    cbar_ptcl = ( 8.0 .* met.T .* k_B ./ ( 3.1415 .* sd.Mass( imode ) ) ) .^ 0.5;
    Kn = XLM ./ sd.dg( imode );
    Cc = 1 + Kn .* ( 1.257 + 0.4 .* exp( -1.1 ./ Kn ) );
    diff_ptcl = k_B .* met.T .* Cc ./ ( 3.0 .* pi .* AMU .* sd.dg( imode ) ); %m2 s-1
    
    
    %Equation A15 of Binkowski and Shankar (1995) for the second and third
    %moments of a lognormal distribution of arbitrary size
    M1 = sd.Num(imode) .* sd.dg(imode) .* exp( 0.5 .* (log(sd.sg(imode))).^2 );
    
    GNC1 = ( vap.dv + diff_ptcl );
    GNC2 = ( vap.dv + diff_ptcl ) .* sd.Num(imode);
    GNC3 = ( vap.dv + diff_ptcl ) .* ( M1 + sd.Num(imode) .* vap.diam );
    
    GFM1 = alpha .* sqrt( vap.cbar.^2 + cbar_ptcl.^2 ) ...
           .* ( sd.Num(imode) );
    GFM2 = alpha .* sqrt( vap.cbar.^2 + cbar_ptcl.^2 ) ...
           .* ( M1 + 2.0 .* sd.Num(imode) .* vap.diam );
    GFM3 = alpha .* sqrt( vap.cbar.^2 + cbar_ptcl.^2 ) ...
           .* ( sd.M2(imode) + 2.0 .* M1 .* vap.diam + sd.Num(imode) .* vap.diam .^ 2 );
    
    knud1 = ( 6.0 .* GNC1 ) ./ GFM1;
    knud2 = ( 6.0 .* GNC2 ) ./ GFM2;
    knud3 = ( 6.0 .* GNC3 ) ./ GFM3;
    
    beta1 = ( 1.0 + knud1 ) ./ ( 1.0 + 1.77 .* knud1 + 4../3. .* knud1.^2 );
    beta2 = ( 1.0 + knud2 ) ./ ( 1.0 + 1.77 .* knud2 + 4../3. .* knud2.^2 );
    beta3 = ( 1.0 + knud3 ) ./ ( 1.0 + 1.77 .* knud3 + 4../3. .* knud3.^2 );
    
    %Implement eqn A13
    Ifac( 1,imode ) = pi ./ 4 .* 4../3. .* beta1 .* GFM1;
    Ifac( 2,imode ) = pi ./ 4 .* 4../3. .* beta2 .* GFM2;
    Ifac( 3,imode ) = pi ./ 4 .* 4../3. .* beta3 .* GFM3;
    
end

%Set Growth Factors to 0 if the mode is Deactivated
Ifac = Ifac .* repmat(sd.Active,3,1);

%Calculate fractional growth to each mode normalize by total growth
omega(1,:) = Ifac(1,:) ./ sum( Ifac(1,:) );  %First Moment Growth Fraction
omega(2,:) = Ifac(2,:) ./ sum( Ifac(2,:) );  %Second Moment Growth Fraction
omega(3,:) = Ifac(3,:) ./ sum( Ifac(3,:) );  %Third Moment Growth Fraction

end


%%% Function for Calculating the Condensation Growth Rate during this step
function [Ifac, omega] = get_integ_growth_fac4( sd, vap, met )

%This function calculates the size-dependent growth rates for each mode.
%Its based on papers by Whitby et al. 1991, and Binkowski and Shankar 1995
%
% Ben Murphy September 2015
%
%  sd - size distribution properties
%  dv - molecular diffusivity of condensing vapor
%  cbar - kinetic velocity of condensing vapor
%  Ifac - size-dependent term in condensation rate
%  omega - fractional mass transfer to/from each mode

pi = 3.1415;
k_B = 1.3806e-23; %Boltzmann Constant
AMU = 1.458E-6 .* met.T .* met.T .^2 ./ ( met.T + 110.4 ); %Dynamic Viscosity kg m-1 s-1
XLM = 6.6328E-8 .* 101325 .* met.T ./ ( 298 .* met.P ); %Mean Free Path of Air [m]
alpha = 0.1; %accomodation coefficient
Ifac  = zeros( 3,sd.nmode );
omega = zeros( 3,sd.nmode );

for imode = 1:sd.nmode

    %Determine points for integration
    z = linspace(-2.5,2.5,101);
    %Determine Diamters at each point [ m ]
    xDg = exp( sqrt(z(1:end-1).*z(2:end)) .* sign(z(1:end-1)) .* sqrt(2.) ...
            .* log( sd.sg(imode) ) ) .* sd.dg(imode);
    %Get number of Particles in each section
    xN  = sd.Num(imode) ./ 2 .* ( erf( z(2:end) ) - erf( z(1:end-1) ) );
    
    for iptcl = 1:length(xN)
    
%         % Kelvin effect
%         Ke(iptcl) = exp(2.0 .* vap.MW .* vap.sigma1 ./ R./T ./ ...
%                     sd.ro ./ ( xDg(iptcl)./2 ) );
%         % Mass based! Remember to fix back!!
%         peq(iptcl) = psat.*Ke(iptcl);
        
        % Transitional correction
        % Knudsen number
        Kn(iptcl) = XLM ./ xDg(iptcl);
        % Fuchs and Sutugin transition regime correction
        beeta(iptcl) = (1.0 + Kn(iptcl))./ ...
            (1.0 + (4./alpha./3 + 0.377).*Kn(iptcl) + 4.*Kn(iptcl).^2./alpha./3);
        
        Cs6(iptcl) = xN(iptcl) .* 2.*pi .* vap.dv .* beeta(iptcl) .* xDg(iptcl) .^ 4;
        Cs4(iptcl) = xN(iptcl) .* 2.*pi .* vap.dv .* beeta(iptcl) .* xDg(iptcl) .^ 2;
        Cs3(iptcl) = xN(iptcl) .* 2.*pi .* vap.dv .* beeta(iptcl) .* xDg(iptcl) ;
        Cs2(iptcl) = xN(iptcl) .* 2.*pi .* vap.dv .* beeta(iptcl) ;
        Cs1(iptcl) = xN(iptcl) .* 2.*pi .* vap.dv .* beeta(iptcl) ./ xDg(iptcl);
    end
    
    %Implement eqn A13
    Ifac( 1,imode ) = sum(Cs1);
    Ifac( 2,imode ) = sum(Cs2);
    Ifac( 3,imode ) = sum(Cs3);
    Ifac( 4,imode ) = sum(Cs4);
    Ifac( 5,imode ) = sum(Cs6);
        
end

%Set Growth Factors to 0 if the mode is Deactivated
Ifac = Ifac .* repmat(sd.Active,5,1);

%Calculate fractional growth to each mode normalize by total growth
omega(1,:) = Ifac(1,:) ./ sum( Ifac(1,:) );  %Second Moment Growth Fraction
omega(2,:) = Ifac(2,:) ./ sum( Ifac(2,:) );  %Second Moment Growth Fraction
omega(3,:) = Ifac(3,:) ./ sum( Ifac(3,:) );  %Third Moment Growth Fraction
omega(4,:) = Ifac(4,:) ./ sum( Ifac(4,:) );  %Third Moment Growth Fraction
omega(5,:) = Ifac(5,:) ./ sum( Ifac(5,:) );  %Third Moment Growth Fraction

end

