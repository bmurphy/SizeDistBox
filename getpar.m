%%% Function for Recalculating Modal Parameters online
function sd = getpar( sd, limit_sg )

%Calculate Mass, SA, Dg and sigma from the 0, 2nd and 3rd moments of the
%distribution
global M2_Grow_Method

pi = 3.1415;
dgmin = 1.0e-9;
sgmin = 1.01;
sgmax = 2.50;

if (~limit_sg)
    if strcmp( M2_Grow_Method, 'M1' )
        lnsigsq = 1/3 .* log( sd.M3 ) + 2/3 .* log( sd.Num ) - log( sd.M1 );
    elseif strcmp( M2_Grow_Method, 'M4' )
        lnsigsq = 1/2 .* log( sd.M4 ) - 2/3 .* log( sd.M3 ) + 1/6 * log( sd.Num );
    elseif strcmp( M2_Grow_Method, 'M6' )
        lnsigsq = 1/9 .* log( sd.Num ) + 1/9 .* log( sd.M6 ) - 2/9 * log( sd.M3 );
    else
        lnsigsq = 1/3 .* log( sd.Num ) + 2/3 .* log( sd.M3 ) - log( sd.M2 );
    end

    sd.sg = exp( sqrt(lnsigsq) );
    dgcu  = sd.M3 ./ ( sd.Num .* exp( 9/2 .* lnsigsq ) );
    sd.dg = dgcu .^ (1/3);
else
    
    if strcmp( M2_Grow_Method, 'M1' )
        xfsum = 1/3 .* log( sd.M3 ) + 2/3 .* log( sd.Num );
        lxfm2 = log( sd.M1 );
    elseif strcmp( M2_Grow_Method, 'M4' )
        xfsum = -2/3 .* log( sd.M3 ) + 1/6 .* log( sd.Num );
        lxfm2 = -1/2 .* log( sd.M4 );
    elseif strcmp( M2_Grow_Method, 'M6' )
        xfsum = 1/9 .* log( sd.Num ) - 2/9 .* log( sd.M3 );
        lxfm2 = -1/9 .* log( sd.M6 );
    else
        xfsum = 1/3 .* log( sd.Num ) + 2/3 .* log( sd.M3 );
        lxfm2 = log( sd.M2 );
    end
        
    l2sg  = xfsum - lxfm2;
    
    l2sg = max( l2sg, repmat(log(sgmin),1,sd.nmode) );
    l2sg = min( l2sg, repmat(log(sgmax),1,sd.nmode) );
    
    lxfm2 = xfsum - l2sg;
    
    %Solve for Sigma and Dg
    sd.sg = exp( sqrt(l2sg) );
    sd.dg = max( repmat(dgmin,1,sd.nmode), ( sd.M3./ (sd.Num .* exp( 4.5 .* l2sg ) )).^(1/3) );
    
    %Assign M1 and M2 depending on which is the free variable
    if strcmp( M2_Grow_Method, 'M1' )
        sd.M1 = exp( lxfm2 );
        sd.M2 = sd.Num .* sd.dg .^ 2 .* exp( 2 .* l2sg ); 
        sd.M4 = sd.Num .* sd.dg .^ 4 .* exp( 8 .* l2sg ); 
        sd.M6 = sd.Num .* sd.dg .^ 6 .* exp( 18 .* l2sg ); 
    elseif strcmp( M2_Grow_Method, 'M4' )
        sd.M1 = sd.Num .* sd.dg .* exp( 0.5 .* l2sg ); 
        sd.M2 = sd.Num .* sd.dg .^ 2 .* exp( 2 .* l2sg ); 
        sd.M4 = exp( lxfm2 .* -2 ); 
        sd.M6 = sd.Num .* sd.dg .^ 6 .* exp( 18 .* l2sg ); 
    elseif strcmp( M2_Grow_Method, 'M6' )
        sd.M1 = sd.Num .* sd.dg .* exp( 0.5 .* l2sg ); 
        sd.M2 = sd.Num .* sd.dg .^ 2 .* exp( 2 .* l2sg ); 
        sd.M4 = sd.Num .* sd.dg .^ 4 .* exp( 8 .* l2sg ); 
        sd.M6 = exp( lxfm2 .* -9 ); 
    else
        sd.M1 = sd.Num .* sd.dg .* exp( 0.5 .* l2sg ); 
        sd.M2 = exp( lxfm2 );
        sd.M4 = sd.Num .* sd.dg .^ 4 .* exp( 8 .* l2sg ); 
        sd.M6 = sd.Num .* sd.dg .^ 6 .* exp( 18 .* l2sg ); 
    end
        
    
end

sd.SA = sd.M2 .* pi;
sd.Mass = sd.M3 .* pi./6 .* sd.ro;

end


