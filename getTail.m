function [AAA, Dij] = getTail( sd, lmode, bmode )

%Calculate the z coordinate for the threshold diameter relevant for this
%mode
Dij = sd.Threshold( lmode );

xlsgi = log( sd.sg(lmode) );
xlsgj = log( sd.sg(bmode) );
Ni    = sd.Num(lmode);
Nj    = sd.Num(bmode);
Dgi   = sd.dg(lmode);
Dgj   = sd.dg(bmode);

%Calculate intermediate values for quadratic solution
AAA = log( Dij ./ Dgi ) ./ (sqrt(2) .* xlsgi );


end


