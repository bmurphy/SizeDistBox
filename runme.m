%This is the run script for the Size Distribution Evolution Code. The code
%here will evolve in multiple stages:
%      -Single Box with Four Size Modes that Grow by fixed condensation
%       using the mode Merging Algorithm (MMA)
%      -Replace MMA with DMM
%      -Allow Size Classes to be Mobile and out of Order
%      -Introduce Emissions (or advected material) at several time points 
%       that don't correspond to already present modes

clear variables
global M2_Grow_Method

questName = 'SingleMode';
datadir   = ['..\..\Data\SizeDistBox\',questName];
if ~exist( datadir,'dir' ); mkdir( datadir ); end

% Initialize Size Distirbution Properties         
aeroType  = 'MovingSect';  %Size Distribution Representation [Modal | TOMAS | MovingSect ]
distName  = 'Evap';     %Initial Distribution Look and Feel
mergeType = 'None';        %Type of Reallocation to Do [DMM | MMA | KOR]
M3_Grow_Method = 'Conc';  %Form of Condensation [ Conc | Mass_Growth | Log_Growth | Linear_Growth ]
M2_Grow_Method = 'skew';  %How to evolve the Second Moment [Evolve | Const_Stdev | M1 | M6 | skew] 
[sd, vap] = init_size_dist( distName, mergeType, aeroType );

%Print total Mass
fprintf(1,'\nInitial Conditions\n')
fprintf(1,'                    Sum       Modes:\n')
fprintf(1,'Number [N m-3]: \t%7.3e %7.3e %7.3e %7.3e %7.3e\n', sum(sd.Num), sd.Num)
fprintf(1,'Diameter [m]: \t\t%7.3e %7.3e %7.3e %7.3e\n',sd.dg ) 
fprintf(1,'Stnd Dev :    \t\t%7.3e %7.3e %7.3e %7.3e\n',sd.sg(end,:))
fprintf(1,'Surface Area [m2 m-3]: \t%7.3e %7.3e %7.3e %7.3e %7.3e\n',sum(sd.SA), sd.SA)
fprintf(1,'Mass [ug m-3]: \t\t%7.3e %7.3e %7.3e %7.3e %7.3e\n',sum(sd.Mass),sd.Mass)

%Set Simulation Variables
condrate = 0.1; %Set Condensation Rate [ug m-3 s-1]
tout = 700; %Time Duration of Simulation [s]
dt   = 1;   %Time Step Length [s]
          %Since CMAQ currently takes Euler Steps, that's what I'm going
          %to do here too.
          %Below 90 s time steps and the Moving Sectional method won't
          %condense enough sulfuric acid between synchronization time
          %steps.

%Integrate Through Time and Plots Results
if strcmp( sd.aeroType,'Modal' )
    %Call Time-Integration Routine
    sdResult = evolve_modal_dist(sd, vap, condrate, tout, dt, ...
        mergeType, M3_Grow_Method, M2_Grow_Method);
    %Make Plots of the Result
    make_modal_plots
    %Save Results in Quest Folder
    save( [ datadir,'\modal_',mergeType,'_',M3_Grow_Method,'_',M2_Grow_Method,'.mat' ],'sdResult','vap')

elseif strcmp( sd.aeroType,'MovingSect' )
    %Call Time-Integration Routine
    sdResult = evolve_movingSect_dist(sd, vap, condrate, tout, dt, ...
        mergeType, M3_Grow_Method);
    %Make Plots of the Result
    make_movingSect_plots
    %Save Results in Quest Folder
    save( [ datadir,'\MovingSect.mat' ],'sdResult','vap')

    
elseif strcmp( sd.aeroType,'TOMAS' )
    %Call Time-Integration Routine
    sdResult = evolve_tomas_dist(sd, vap, condrate, tout, dt, ...
        mergeType, M3_Grow_Method);
    %Make Plots of the Result
    make_tomas_plots
    
elseif strcmp( sd.aeroType,'Analytical' )
    %Call Time-Integration Routine
    sdResult = evolve_analytical_dist(sd, vap, condrate, tout, dt, ...
        mergeType, M3_Grow_Method);
    %Make Plots of the Result
    make_analytical_plots
end


