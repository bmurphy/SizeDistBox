%First Print total Mass (need to fix the output here)
fprintf(1,'\nFinal Conditions\n')
fprintf(1,'Number [N m-3]: \t%7.3e %7.3e %7.3e %7.3e %7.3e\n',...
    sdResult.Num_sect(end,:), sum(sdResult.Num_sect(end,:)))
fprintf(1,'Diameter [m]: \t\t%7.3e %7.3e %7.3e %7.3e\n',...
    sdResult.dg_sect(end,:))
fprintf(1,'Surface Area [m2 m-3]: \t%7.3e %7.3e %7.3e %7.3e %7.3e\n',...
    sdResult.SA_sect(end,:), sum(sdResult.SA_sect(end,:)))
fprintf(1,'Mass [ug m-3]: \t\t%7.3e %7.3e %7.3e %7.3e %7.3e\n',...
    sdResult.Mass_sect(end,:), sum(sdResult.Mass_sect(end,:)))

%Make a bunch of Size Distribution plots
set_plot_props

figTitle = ['ParticleBox_' distName '_' mergeType];

%%%% Loop through time and plot the size distribution %%%%
pi = 3.1415;
tvec = 0:dt:tout;
nt   = length(tvec);
xdg  = logspace(-9, -2, 200); %Diameter axis
xdgMode = repmat(xdg, sdResult.nmode+1,1);
Dlim = sd.Dplot;
Nlim = [1e0,3e10]; %[N/m3/m
Slim = [1e-15,2e-3];  %[m2/m3/m]
Mlim = [1e-6,1500]; %[ug/m-3/m]
scolor = ['k','g','c','m'];

plot_times = floor(linspace(1,nt,5));
plot_times = [[1:2:10], floor(linspace(11,nt,10))];

for it = plot_times(end:-1:1)

    F1 = makeFig(figTitle, 'skinny_page','landscape');
    
    %Convert Size Distribution Parameters to a Number Trend (dNdlog10Dp)
    dNdlog10dp = get_ndist(xdg, sdResult, it);
    
    %Convert Number Distribution to Surface Area and Plot
    dSdlog10dp = pi .* xdgMode.^2 .* dNdlog10dp;
        
    %Convert Number Distribution to Mass and Plot
    dMdlog10dp = sd.ro .* pi./6 .* xdgMode.^3 .* dNdlog10dp;

    xNcut = repmat(sdResult.Dcut_grow(it,:,1), 2,1);
    xScut = repmat(sdResult.Dcut_grow(it,:,2), 2,1);
    xMcut = repmat(sdResult.Dcut_grow(it,:,3), 2,1);
    ycut = repmat([0;1e12], 1,size(xNcut,2));
    
    %Plot Number Distribution
    P1 = subplot(1,3,1);
    plot( log10( xdg ), dNdlog10dp(1,:), 'color','b','marker','none','linestyle','-','linewidth',2)
    hold on
    plot( log10(xNcut), ycut, 'color','r','marker','none','linestyle','--','linewidth',1)
    for imode = 1:sdResult.nmode
        plot( log10( xdg ), dNdlog10dp(imode+1,:), 'color',scolor(imode),'marker','none','linestyle','--','linewidth',2)
    end
    bounds = get(gca, 'OuterPosition');
    set(gca, 'OuterPosition', [bounds(1:3) .95])
    
    xlabel('log_{10} [Diameter (m)]')
    ylabel('dNdlog10dp')
    ylim(Nlim)
    xlim(Dlim)
    set(gca,'yscale','log')
    
    
    %Plot Surface Distribution
    P2 = subplot(1,3,2);
    plot( log10( xdg ), dSdlog10dp(1,:), 'color','b','marker','none','linestyle','-','linewidth',2)
    hold on
    plot( log10(xScut), ycut, 'color','r','marker','none','linestyle','--','linewidth',1)
    for imode = 1:sdResult.nmode
        plot( log10( xdg ), dSdlog10dp(imode+1,:), 'color',scolor(imode),'marker','none','linestyle','--','linewidth',2)
    end
    bounds = get(gca, 'OuterPosition');
    set(gca, 'OuterPosition', [bounds(1:3) .95])
    
    
    xlabel('log_{10} [Diameter (m)]')
    ylabel('dSdlog10dp')
    ylim(Slim)
    xlim(Dlim)
    set(gca,'yscale','log')

    %Plot Mass Distribution
    P3 = subplot(1,3,3);
    plot( log10( xdg ), dMdlog10dp(1,:), 'color','b','marker','none','linestyle','-','linewidth',2)
    hold on
    plot( log10(xMcut), ycut, 'color','r','marker','none','linestyle','--','linewidth',1)
    for imode = 1:sdResult.nmode
        plot( log10( xdg ), dMdlog10dp(imode+1,:), 'color',scolor(imode),'marker','none','linestyle','--','linewidth',2)
    end
    bounds = get(gca, 'OuterPosition');
    set(gca, 'OuterPosition', [bounds(1:3) .95])
    
    
    xlabel('log_{10} [Diameter (m)]')
    ylabel('dMdlog10dp')
    ylim(Mlim)
    xlim(Dlim)
    set(gca,'yscale','log')

    subtitle(['Particle Size Distributions after ' num2str(tvec(it)) ' sec'],'fontsize',14);
       
    for imode = 1:sdResult.nmode
       annotation('textbox',[0.91,0.2*imode,0.08,0.1],'string',{[num2str(imode),'. GM2: ', ...
                     num2str(sdResult.GM2(it,imode))],[' GM3: ',num2str(sdResult.GM3(it,imode))]})
    end
end


%%%% Make Plots that look like Whitby et al. 2002

F2 = makeFig(figTitle, 'skinny_page','landscape');

xNcut = repmat(sdResult.Dcut_grow(it,:,1), 2,1);
xScut = repmat(sdResult.Dcut_grow(it,:,2), 2,1);
xMcut = repmat(sdResult.Dcut_grow(it,:,3), 2,1);
ycut = repmat([0;1e12], 1,size(xNcut,2));

%Plot Number Distribution
P1 = subplot(1,3,1);
plot( tvec, sdResult.Num ./1.e6, 'marker','none','linestyle','-','linewidth',2)
hold on
bounds = get(gca, 'OuterPosition');
set(gca, 'OuterPosition', [bounds(1:3) .95])
    

xlabel('Time [s]')
ylabel('Number Concentration [#/cc]')
ylim([0.0, 1.4e4])
xlim([tvec(1), tvec(end)])
set(gca,'yscale','linear')


%Plot Mean Size
P2 = subplot(1,3,2);
plot( tvec, sdResult.dg, 'marker','none','linestyle','-','linewidth',2)
hold on
bounds = get(gca, 'OuterPosition');
set(gca, 'OuterPosition', [bounds(1:3) .95])    

xlabel('Time [s]')
ylabel('Mean Size [m]')
ylim([1.0e-7, 1.0e-3])
xlim([tvec(1), tvec(end)])
set(gca,'yscale','log')


%Plot Standard Deviation
P3 = subplot(1,3,3);
plot( tvec, sdResult.sg, 'marker','none','linestyle','-','linewidth',2)
hold on
bounds = get(gca, 'OuterPosition');
set(gca, 'OuterPosition', [bounds(1:3) .95])

xlabel('Time [s]')
ylabel('Standard Deviation [-]')
ylim([1,2])
xlim([tvec(1), tvec(end)])
set(gca,'yscale','linear')


subtitle(['Modal Properties After ' num2str(tvec(end)) ' sec'],'fontsize',14);

    


