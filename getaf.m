function [AAA, Dij] = getaf( sd, lmode, bmode )

%Calculate the overlap diameter of two modes, one little (lmode) and one
%big (bmode). This subroutine is applied already in CMAQ.
Dij = NaN;

xlsgi = log( sd.sg(lmode) );
xlsgj = log( sd.sg(bmode) );
Ni    = sd.Num(lmode);
Nj    = sd.Num(bmode);
Dgi   = sd.dg(lmode);
Dgj   = sd.dg(bmode);

%Calculate intermediate values for quadratic solution
alpha = xlsgi ./ xlsgj;
Yji   = log( Dgj ./ Dgi ) ./ ( sqrt(2) .* xlsgi );
L     = log( alpha .* Nj ./ Ni );

%Calculate coefficients for quadratic equation
AA = 1.0 - alpha .* alpha;
BB = 2.0 .* Yji .* alpha .* alpha;
CC = L - Yji .* Yji .* alpha .* alpha;
DISC = BB .* BB - 4.0 .* AA .* CC;

%If roots are imaginary, return a negative getaf so that no mode merging
%takes place
if (DISC < 0 ) 
    AAA = -5.0;
    return
end

%Solve quadratic equation
QQ = -0.5 .* (BB + sign(BB) .* sqrt(DISC) );
AAA = CC ./ QQ;

if (isreal(AAA))
    Dij = exp( AAA .* sqrt(2) .* xlsgi ) .* Dgi; %[m]
end

end


