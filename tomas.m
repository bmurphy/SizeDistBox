function sd = tomas( sd, vap, condrate, dt, Dp_Grow_Method )
%This function implements the Two-MOment Aerosol Sectional method (TOMAS)
%originally formulated by Peter Adams, based on work by Tzivion et al.,
%1989.

%%% Input parameters
% condrate - rate of material condensing to particles [ug m-3 s-1]
% sd - structure of aerosol concentrations and properties

Tau_form = 'exponential';  % constant | exponential

%Load Local Variables
R = 8.314;
T = 298;  %K
P = 101325.0; % Total pressure [Pa]
Dp = sd.dp;   %m
Neps = 1.0e-20;  %Min N Conc [ N m-3 ]
Nk = max( Neps, sd.Num_sect);  %N m-3
Mk = sd.Mass_sect .* 1.0e-9; %kg m-3
zeta13 = 0.98483;  %Weird Fudge Factor
rho = sd.ro.*1.0e-9;%kg m-3
MW  = vap.MW;       %kg mol-1
alpha = 1.0; %accomodation coeffieicent

%Calculate the bin boundaries
Dpk = [sd.dpLo, sd.dpHi(end)];
xk = pi./6 .* Dpk.^3 .* rho; %kg m-3

%Set Up Amount of Vapor to Condense (assume nonvolatile vapor)
switch Dp_Grow_Method
    case 'Conc'
        %Vapor Concentration is passed directly
        Cgas = vap.conc .* 1.0e-9; %kg m-3
    otherwise
        %Vapor concentration is inferred from condensation rate
        Cgas = condrate .* dt .* 1.0e-9; % kg m-3
end

%First Calculate Chemical-Dependent Parameters
mu = 2.5277e-7 .* T .^ 0.75302;
% Dv = gasdiff( T, P, vap.MW, Sv );
Dv = vap.dv;    %m2 s-1
tj = 2 .* pi .* Dv .* MW ./R./T;  
tj = 2 .* pi .* Dv; % m2 s-1 

%Calculate size-dependent parameters (Fuchs Correction) for each size
%bin. The original TOMAS does not incorporate a Kelvin effect. Go ahead an
%incorporate one anyway.
lamda = 2.0 .* mu / (P .* sqrt(8.0*0.0289/(pi.*R.*T)));
Kn = 2.0 .* lamda ./ Dp;
beta = (1.+Kn) ./ (1. + 2.0.*Kn.*(1.0+Kn) ./ alpha);
tk = (6./(pi.*rho)).^(1.0./3.0) .* beta;

%Calculate Total Vapor Condensation Sink (Anchored to Continuum Regime)
sK = tj .* sum( Nk .* tk .* ( Mk ./ Nk ).^(1.0/3.0) ); % .*zeta13;

%Start Sub time-stepping
sync_dt = dt;
nt = 0.0;
while nt < sync_dt
    
    %Calculate tau driving forces to be sent to tmconds for doing the mass- and
    %number-conserving algorithm. Tau is the time integral of the collection
    %kernel times the saturation ratio. 
    switch Tau_form
        case 'constant'
            delC = (Cgas - 0.0);  %Pressure Differential in Pascals
            atau = tj .* tk .* delC .* dt;  %Constant Tau*
        case 'exponential'
            atau = tj .* tk .* (Cgas ./ sK ) .* ( 1.0 - exp(-1.0 .* sK .* dt ) );
    end
    delC = (Cgas - 0.0);  %Pressure Differential in Pascals
    atauc = tj .* tk .* delC .* dt;  %Constant Tau*
    ataue = tj .* tk .* (Cgas ./ sK) .* ( 1.0 - exp(-1.0 .* sK .* dt ) );
    [sK, ( 1.0 - exp(-1.0 .* sK .* dt ))]
    close all
    plot(log10(Dp),log10(atauc),log10(Dp),log10(ataue))
    legend('Constnat','Exponential')
    dt
    %Check various criteria to see if time step should be reduced
    tr = 1.0;  %Default is to not reduce the time step
    %Check all particle sizes for fast changes
    mc = 0.0;
    mc = Mk ./ (Nk+Neps);
    flower = Nk > Neps;
    %weed = mc./xk(1:end-1) > 1.0e-3;
    weed1 = flower & abs(atau./(mc+1.0e-20).^(2./3.)) > 10;
    ttr = nanmax( abs(atau(weed1))./mc(weed1).^(2./3.) ./ 0.05 );
    if ~isempty( ttr ); tr = max( tr,ttr ); end
    %Adjust time step for stability of exponential loss term
    if strcmp( Tau_form,'exponential' ) && exp(-1.0*sK*dt) < 0.55
%         tr = max( tr,-2.0*dt*sK/log(0.55) );
    end
        
    tr = min( tr,2.0 );
    if tr == 1.0
        %Run Condensation and Loop 
        %Check to make sure all of the particle sizes are within the correct bounds
        %and that no masses or numbers have gone negative.
        [Nk, Mk] = mnfix_PSSA( Nk, Mk, xk );
        
        
        %Execute Two-Moment Sectional Method
        [Nk2, Mk2] = tmcond( atau, xk, Nk, Mk );
        
        %Check for Number Errors
        if (sum(Nk2)-sum(Nk))/sum(Nk) > 1.0d-4
            fprintf(1,'There''s an error in TOMAS. Number not conserved: %d %d',sum(Nk),sum(Nk2))
        end
        
        %Analyze Vapor Change
        Cgas_diff = (sum(Mk2)-sum(Mk))./ Cgas
%         dgas = Mk - Mk2;
%         if sum(dgas) < 0.0
%             tgas = max( sum(dgas), -Cgas ); %limit vapor change
%             Mk2 = Mk + (-dgas)./sum(dgas).*tgas;
%         end
        Cgas_diff = (sum(Mk2)-sum(Mk))./ Cgas
        Cgas = Cgas + sum(Mk) - sum(Mk2);
        
        %Increment Time Variable
        if nt < sync_dt
            nt = min( nt+dt, sync_dt );
            dt = dt .* 2;
        else
            nt = sync_dt + dt;
        end
    else
       %Reduce the time step and Loop
       dt = dt ./ tr;
        
    end

end %Looping time step

dt = sync_dt;  %Reset time-stepping parameter

mass_diff = 1.0e9.*sum(Mk2) - sum(sd.Mass_sect)

%Update Aerosol Concentration Variable
sd.Num_sect = Nk2;  %N m-3
sd.Mass_sect = Mk2 .* 1.0e9; %ug m-3

end

%%%%%%%%%%%%%%%%%%%%%% Diffusivity %%%%%%%%%%%%%%%%%%%
%Calculate the gas-phase diffusivity of a species with molecular weight MW
%and atomic diffusion volume Sv.
function Dv = gasdiff( T, P, MW, Sv )

mwair = 0.0289;  %kg mol-1
Svair = 20.1;    %

mwf = sqrt( (MW+mwair)/(MW*mwair) );
Svf = (Sv ^(1.0./3.0) + Svair^(1.0./3.0) )^2.0;
gasdiff = 1.0E-7 .* T .^1.75 .* mwf ./ P .* 1.0E5 ./ Svf;

end


%%%%%%%%%%%%%%%%%%%% MNFIX_PSSA %%%%%%%%%%%%%%%%%%%%
%Check to make sure that all particle sizes are within the corresponding
%bounds
function [Nk, Mk] = mnfix_PSSA( Nk, Mk, xk )

Neps = 1.0d-10;
Meps = 1.0d-32;
xkmid = sqrt(xk(1:end-1) .* xk(2:end));

%First Check for Negative Tracers
weed = Nk < Neps & Nk > -1.0d10;
Nk( weed ) = Neps;
Mk( weed ) = Neps .* xkmid(weed);
if ( any( Nk < -1.0d10 ))
    fprintf( 1,'Very large negative number: %d',Nk( Nk < -1.0d10 ) )
    error
end

%Check Total Mass
weed = Mk < Meps;
Nk( weed ) = Neps;
Mk( weed ) = Neps .* xkmid(weed);
    
%Check Component Masses
  %%un-needed right now%%

%Check that particles actually belong in each size bin
xk_lo = xkmid(1);
xk_hi = xkmid(end);

%Check for particles below lowest size bin. If they're too small, then
%conserve the mass and let the number change to fit them into the smallest
%size bin at the midpoint particle mass.
weed = Mk./Nk <= xk_lo;
if any(weed)
    Nk(1) = Nk(1) + sum(Mk(weed) ./ xk_lo);
    Mk(1) = Mk(1) + sum(Mk(weed));
    Nk(weed) = Neps;
    Mk(weed) = Neps .* xkmid(weed);
end

%Check for particles above highest size bin
weed = Mk./Nk >= xk_hi;
if any(weed)
    Nk(end) = Nk(end) + sum(Mk(weed) ./ xk_hi);
    Mk(end) = Mk(end) + sum(Mk(weed));
    Nk(weed) = Neps;
    Mk(weed) = Neps .* xkmid(weed);
end

%Test if any bins are outside their lower or upper boundaries
weed = [0, Mk(2:end)./Nk(2:end) < xk(2:end-1)]  | ...
       [Mk(1:end-1)./Nk(1:end-1) > xk(2:end-1), 0];
if any(weed)
    xkmid = sqrt( xk(1:end-1) .* xk(2:end) );
    for iweed = find(weed)
        %Determine the Closest Upper and Lower Bin Centers
        midhi = find( xkmid > Mk(iweed)./Nk(iweed), 1 );
        midlo = midhi - 1;
        
        %Compute Fraction of Material Staying in the upper bin
        frac_lo_n = ( Nk(iweed)*xkmid(midhi) - Mk(iweed) )./...
                    ( Nk(iweed)* ( xkmid(midhi) - xkmid(midlo)) );
        frac_lo_m = frac_lo_n .* Nk(iweed) .* xkmid(midlo) ./ Mk(iweed);
        
        %Reset Number and Mass of this wrong Bin
        tvar_n = Nk(iweed);
        Nk(iweed) = Neps;
        tvar_m = Mk(iweed);
        Mk(iweed) = Neps .* xkmid(iweed);
        
        %Distribute Fractions
        Nk(midlo) = Nk(midlo) + frac_lo_n .* tvar_n;
        Nk(midhi) = Nk(midhi) + (1.0-frac_lo_n) .* tvar_n;
        Mk(midlo) = Mk(midlo) + frac_lo_m .* tvar_m;
        Mk(midhi) = Mk(midhi) + (1.0-frac_lo_m) .* tvar_m;
        
    end
end

%Check one last time
weed = Mk./Nk < xk(1:end-1) | Mk./Nk > xk(2:end);
if any(weed)
    find(weed)
    [ [1:length(Mk)]', Mk'./Nk', xk(1:end-1)',xk(2:end)']
    error('Some of the TOMAS sections are either too big or too small')
end
    
end

%%%%%%%%%%%%%%%%%%% DMDT_INT %%%%%%%%%%%%%%%%%%%%%%
function Y = dmdt_int( x0, tau )
%This function projects back in time the change in particle size due to
%condensation/evaporation for a specific size (X0).

%X, Y and tau can be scalars or vectors.
Cnst = 2.0d0/3.0d0;
L0 = 0.0d0;

X = x0.^Cnst + L0;
if (Cnst*tau + X) > 0.0 
    X = sqrt( Cnst*tau + X );
else
    X = 0;
end
Y = X.^3;

weed = tau > 0.0 .* Y < x0;
if any(weed); fprintf(1,'Weird things happening in dmdt_int'); end
weed = tau < 0.0 .* Y > x0;
if any(weed); fprintf(1,'Weird things happening in dmdt_int'); end

end


%%%%%%%%%%%%%%%%%%% TMCOND %%%%%%%%%%%%%%%%%%%%%%%%
function [Nk2, Mk2] = tmcond( tau, xk, Nk, Mk )

%This function actually does the two-moment condensation/evaporation while
%conserving number and mass simultaneously.
% Originally written by Tzivion and Feingold
% Adapted by Peter Adams for TOMAS, and Jae Gun Jung, Win T. and Jeff Pierce for
% later versions.
% Ben Murphy wrote it in Matlab
Neps = 1.0d-20;
Teps = 1.0d-40;
xkmid = sqrt(xk(1:end-1) .* xk(2:end));
nbins = length(Nk);
 
%Check for small numbers
weed = Nk < Neps;
Nk(weed) = Neps;
Mk(weed) = Neps .* xkmid(weed);

%PJA noticed that sometimes the average bin mass can be just above the
%bin boundary. Transfer 10% of mass and number up to the bin.
weed = Mk(1:end-1)./Nk(1:end-1) > xk(2:end-1);
Mk(find(weed)+1) = Mk(find(weed)+1) + Mk(weed)*0.1;
Nk(find(weed)+1) = Nk(find(weed)+1) + Nk(weed)*0.1;
Mk(weed) = Mk(weed)*0.9;
Nk(weed) = Nk(weed)*0.9;

%Only do Transfer when significant forcing is available
if max(tau) < Teps; Mk2 = Mk; Nk2 = Nk; return; end

%Initialize new Mass and Number Parameter
Mk2 = zeros(1,nbins);
Nk2 = zeros(1,nbins);

%Calculate Size Bounds for Lagrangian Algorithm
AVG_X = Mk ./ Nk;
XX = xk(1:end-1) ./ AVG_X;
XI = 0.5d0 + XX .* (1.5d0 - XX);
W1 = sqrt( 12.d0.* (XI-1.d0)).* AVG_X;
W2 = min( xk(2:end) - AVG_X, AVG_X - xk(1:end-1) );
WTH = W1.*0.5 + W2.*(1.d0-0.5);

if (XI < 1.d0); error( 'XI is less than 1.0. This is problematic in tmcond'); end
if (WTH > 1.d0); error( 'WTH is greater than 1.0. THis is problematic in tmcond'); end
    
XU = AVG_X + WTH.* 0.5d0;
XL = AVG_X - WTH.* 0.5d0;

%Calculate future Upper Dp Boundary (YU) and Lower Dp Boundary (YL). These
%are the boundaries of the particles that will grow or shrink into the
%boundaries defined by XU and XL.
YU = dmdt_int( XU, tau );
if YU > xk(end); YU = xk(end); end
YL = dmdt_int( XL, tau );
%If any bins shrink below the lowest bin, add them to the lowest bin
%weed = YL < xk(1);
%Nk2(1) = Nk2(1) + Nk(weed);
%Mk2(1) = Mk2(1) + Nk(weed).* sqrt( YU(weed) .* YL(weed) );

%Remap the particles from the YL to YU bins to the new XL to XU bins.
[Nk2, Mk2] = tomas_remap(Nk, Mk, Nk2, Mk2, xk, YL, YU );

% sum(Nk2) ./ sum(Nk)
%sum(Mk2) ./ sum(Mk)

end


%%%%%%%%%%%%%%%%%%%%%%%%%% tomas_remap %%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Nk2, Mk2] = tomas_remap( Nk,Mk,Nk2,Mk2,xk,YL,YU )

%This function does the remapping for the tomas approach
nbins = length(Nk);
DYI  = 1.0d0 ./ (YU-YL);

for ibin = 1:nbins  %Loop Mass Transfer Destination Bins
   
    istrt = 1;  %Start Remapping in Bin 1
    iend  = nbins; %Finish in the last bin
    %if tau(ibin) > 0.0; istrt = ibin; end  %Condensation - start remapping from current bin
    %if tau(ibin) < 0.0; iend  = ibin; end  %Evaporation  - finish remapping in current bin
            
    for jbin = istrt:iend  %Loop Over Fixed Bins
        if xk(jbin+1) >= YL(ibin) && xk(jbin) <= YU(ibin)
            %At least part of the destination bin is smaller than this fixed bin
                lb = max( xk(jbin),   YL(ibin) ); %Get the relevant lower-boud
                ub = min( xk(jbin+1), YU(ibin) ); %Get the relevant upper-bound
                Nk2(jbin) = Nk2(jbin) + Nk(ibin) .* ( ub-lb ).*DYI(ibin);
                Mk2(jbin) = Mk2(jbin) + Nk(ibin) .* sqrt(YL(ibin).*YU(ibin)) .* ( ub-lb ).*DYI(ibin);
                                                 %.* sqrt( lb.*ub );
        end
    end            
    
end

%Take care of Mass that's out of range too high
weed = YU > xk(nbins+1);
if any(weed)
    lb = max( xk(nbins+1), YL(weed) );
    ub = YU( weed );
    Nk2( nbins ) = Nk2( nbins ) + Nk(weed).* ( ub-lb ) .* DYI(weed);
    Mk2( nbins ) = Mk2( nbins ) + Nk(weed).* sqrt(YL(ibin).*YU(ibin)) .* ( ub-lb ) .* DYI(weed); %.*sqrt( lb.*ub);
end

%Take care of Mass that's out of range too low
weed = YL < xk(1);
if any(weed)
    lb = YL(weed);
    ub = min( xk(1), YU( weed ));
    Nk2( 1 ) = Nk2( 1 ) + Nk(weed).* ( ub-lb ) .* DYI(weed);
    Mk2( 1 ) = Mk2( 1 ) + Nk(weed).* sqrt(YL(ibin).*YU(ibin)) .* ( ub-lb ) .* DYI(weed); %.*sqrt( lb.*ub);
end

end



