function [ sdResult ] = evolve_tomas_dist( sd, vap, condrate, tout, dt, ...
    mergeType, Dp_Grow_Method )
%This is the time integration routine that simulates the evolution of the
%particle size distribution. It inputs a modal distribution, converts it to
%sectional and advances it in time.

%Ben Murphy - Feb 2016

%Initialize Output Variables
tvec = dt:dt:tout;
nt   = length(tvec);

%Initialize sdResult Vector
sdResult.nbin  = [sd.nbin];
sdResult.Num_sect = [sd.Num_sect; zeros( nt,sd.nbin )];
sdResult.dp       = [sd.dp; zeros( nt,sd.nbin )];
sdResult.SA_sect  = [sd.SA_sect; zeros( nt,sd.nbin )];
sdResult.Mass_sect= [sd.Mass_sect; zeros( nt,sd.nbin )];

%Loop Through Time Dimension
for it = 1:nt
    %Calculate Condensation Growth To each Section (TOMAS)
    sd = tomas( sd, vap, condrate, dt, Dp_Grow_Method);
       
    %[Later] Add Emissions Mode (or Advected Mode) after testing if one of
    %the existing modes will accomodate
    
    %Update Vapor concentration, if necessary
    if strcmp( Dp_Grow_Method,'Conc' )
        vap.conc = vap.conc -(sum(sd.Mass_sect) - sum(sdResult.Mass_sect(it,:),2) );
    end
        
    %Compute Surface Area (m2 m-3)
    sd.SA_sect = sd.Num_sect .* pi .* sd.dp .^ 2;
    
    %Save Variables in Output Structure
    sdResult.Num_sect(it+1,:)  = sd.Num_sect;
    sdResult.dp(it+1,:)        = sd.dp;
    sdResult.SA_sect(it+1,:)   = sd.SA_sect;
    sdResult.Mass_sect(it+1,:) = sd.Mass_sect;
    
end
end


