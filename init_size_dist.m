%Initialize the size distribution
function [sd, vap] = init_size_dist(modelName, mergeType, aeroType)

pi = 3.1415;
R  = 8.314;  % [J mol-1 K-1]
T  = 298;    % K
Avo = 6.022e23; %Avogadro's Number

%Initialize Vapor Properties
vap.MW = 0.100;    %[kg mol-1]
vap.dv =  9.36e-6 .* (101325 ./ 101325) .* ( T ./ 273.15 ).^(1.75);   %[m2/s]
vap.sigma1 = 0.05; %Surface Tension of Organics
vap.cbar = sqrt( 8.0.* R.*T ./ (pi .* vap.MW ) ); %[m/s]
vap.Cstar = 1.e-12; %Saturation Concentration in ug m-3
vap.rho = 1400; %density. Assume gas and vapor phase are the same ( kg m-3 )
vap.diam = ( vap.MW / ( Avo .* vap.rho ) ) .^ (1../3.);

%Save Aerosol Model Representation Approach
sd.aeroType = aeroType;  %Modal or Sectional

%Set Initial Aerosol Distribution According to one of a few Prescribed
%Cases
switch modelName
    case 'Base'
        vap.Cstar = 1e-1; %New Large Vapor Pressure [ ug m-3 ]
        vap.conc = 5.0e1;  %Set Vapor Concentration [ug m-3]
        sd.ro = 1400 .* 1e9; %Uniform Density [ug m-3]
        
        sd.nmode = 4;
        sd.smode = {'Nuc','Ait','Acc','Cor'};    %Mode Names
        sd.dg    = [8.e-9, 30.e-9, 2.e-7, 2.4e-6]; %Mode Diameters [m]
        sd.sg    = [1.5,   1.5,   1.5,   1.5];   %Mode Standard Deviation
        sd.Num   = [1e4,  1e-3,   1e-6,  1e-8].*1.e6;  %Initial Number in Each Mode [N/m3]
        
        %Calculate Second and Third Moments as well as Surface Area and Mass Values
        sd.M1    = sd.Num .* sd.dg.^1 .* exp( 0.5  .* (log( sd.sg )) .^ 2 ); %[m/m3]
        sd.M2    = sd.Num .* sd.dg.^2 .* exp( 2    .* (log( sd.sg )) .^ 2 ); %[m2/m3]
        sd.M3    = sd.Num .* sd.dg.^3 .* exp( 9./2 .* (log( sd.sg )) .^ 2 ); %[m3/m3]
        sd.M4    = sd.Num .* sd.dg.^4 .* exp( 16./2 .* (log( sd.sg )) .^ 2 ); %[m4/m3]
        sd.M6    = sd.Num .* sd.dg.^6 .* exp( 36./2 .* (log( sd.sg )) .^ 2 ); %[m6/m3]
        sd.SA    = sd.M2 .* pi;  %[m2/m3]
        sd.Mass  = sd.M3 .* pi./6 .* sd.ro; %[ug m-3]
        
        sd.Dplot = [-9, -5];
        
        %Set Parameters for Merge Approaches
        sd.Active= [1  ,   0,     0,     0]; %Active Modes
        sd.dgHome= [1.e-7, 9.e-6, 6.e-6, 3.e-5]; %Home Base Diameters for DMM
        switch mergeType
            case 'KOR'
                sd.Threshold = [1.e-8, 1.e-7, 1.e-6, NaN]; %Thresholds for Merging
            case 'MMA'
            case 'DMM'
        end
        
    case 'Whitby'
        sd.ro = 1400 .* 1e9; %Uniform Density [ug m-3]
        
        sd.nmode = 4;
        sd.smode = {'Nuc','Ait','Acc','Cor'};    %Mode Names
        sd.dg    = [2.e-7, 1.e-6, 6e-6, 3.e-5]; %Mode Diameters [m]
        sd.sg    = [1.5,   1.5,   1.5,   1.5];   %Mode Standard Deviation
        sd.Num   = [1e4,   1.e-4, 1.e-6, 1.e-8].*1.e6;  %Initial Number in Each Mode [N/m3]
        
        %Calculate Second and Third Moments as well as Surface Area and Mass Values
        sd.M1    = sd.Num .* sd.dg.^1 .* exp( 0.5  .* (log( sd.sg )) .^ 2 ); %[m/m3]
        sd.M2    = sd.Num .* sd.dg.^2 .* exp( 2    .* (log( sd.sg )) .^ 2 ); %[m2/m3]
        sd.M3    = sd.Num .* sd.dg.^3 .* exp( 9./2 .* (log( sd.sg )) .^ 2 ); %[m3/m3]
        sd.SA    = sd.M2 .* pi;  %[m2/m3]
        sd.Mass  = sd.M3 .* pi./6 .* sd.ro; %[ug m-3]
        
        sd.Dplot = [-8, -3];
        
        %Set Parameters for Merge Approaches
        sd.Active= [1  ,   0,     0,     0]; %Active Modes
        sd.dgHome= [1.e-7, 1.e-6, 6.e-6, 3.e-5]; %Home Base Diameters for DMM
        switch mergeType
            case 'KOR'
                sd.Threshold = [1.e-8, 1.e-7, 1.e-6, NaN]; %Thresholds for Merging
            case 'MMA'
            case 'DMM'
        end
        
    case 'Korhola'
        sd.ro = 1400 .* 1e9; %Uniform Density [ug m-3]
        
        sd.nmode = 4;
        sd.smode = {'Nuc','Ait','Acc','Cor'};    %Mode Names
        sd.dg    = [4.e-9, 4.e-8, 2e-7,  2.4e-6]; %Mode Diameters [m]
        sd.sg    = [1.59,  1.59,   1.59,   1.59];   %Mode Standard Deviation
        sd.Num   = [1e4,   500,     200,    0.1].*1.e6;  %Initial Number in Each Mode [N/m3]
        
        %Calculate Second and Third Moments as well as Surface Area and Mass Values
        sd.M1    = sd.Num .* sd.dg.^1 .* exp( 0.5  .* (log( sd.sg )) .^ 2 ); %[m/m3]
        sd.M2    = sd.Num .* sd.dg.^2 .* exp( 2    .* (log( sd.sg )) .^ 2 ); %[m2/m3]
        sd.M3    = sd.Num .* sd.dg.^3 .* exp( 9./2 .* (log( sd.sg )) .^ 2 ); %[m3/m3]
        sd.SA    = sd.M2 .* pi;  %[m2/m3]
        sd.Mass  = sd.M3 .* pi./6 .* sd.ro; %[ug m-3]
        
        sd.Dplot = [-9, -5];
        
        %Set Parameters for Merge Approaches
        sd.Active= [1,     1,      1,      1]; %Active Modes
        sd.dgHome= [1.e-7, 9.e-6, 6.e-6, 3.e-5]; %Home Base Diameters for DMM
        switch mergeType
            case 'KOR'
                sd.Threshold = [1.e-8, 1.e-7, 1.e-6, NaN]; %Thresholds for Merging
            case 'MMA'
            case 'DMM'
        end

    case 'Evap'
        vap.Cstar = 1e2; %New Large Vapor Pressure [ ug m-3 ]
        vap.conc = 0.0;  %Set Vapor Concentration [ug m-3]
        sd.ro = 1400 .* 1e9; %Uniform Density [ug m-3]
        
        mass_strt = 80; %ug m-3
        dg_strt = 2.4e-7;
        sg_strt = 1.5;
        num_strt = mass_strt ./ (pi./6 .* sd.ro) ...
                         ./ dg_strt.^3 ./ exp( 9./2 .* (log( sg_strt )) .^ 2 ) ...
                         ./ 1.0e6; %[N/cm3]  
        
        sd.nmode = 4;
        sd.smode = {'Nuc','Ait','Acc','Cor'};    %Mode Names
        sd.dg    = [8.e-9, 30.e-9, 2.e-7, dg_strt]; %Mode Diameters [m]
        sd.sg    = [1.5,   1.5,   1.5,   sg_strt];   %Mode Standard Deviation
        sd.Num   = [1e-2,  1e-3,   1e-5,  num_strt].*1.e6;  %Initial Number in Each Mode [N/m3]
        
        %Calculate Second and Third Moments as well as Surface Area and Mass Values
        sd.M1    = sd.Num .* sd.dg.^1 .* exp( 0.5  .* (log( sd.sg )) .^ 2 ); %[m/m3]
        sd.M2    = sd.Num .* sd.dg.^2 .* exp( 2    .* (log( sd.sg )) .^ 2 ); %[m2/m3]
        sd.M3    = sd.Num .* sd.dg.^3 .* exp( 9./2 .* (log( sd.sg )) .^ 2 ); %[m3/m3]
        sd.M4    = sd.Num .* sd.dg.^4 .* exp( 16./2 .* (log( sd.sg )) .^ 2 ); %[m4/m3]
        sd.M6    = sd.Num .* sd.dg.^6 .* exp( 36./2 .* (log( sd.sg )) .^ 2 ); %[m6/m3]
        sd.SA    = sd.M2 .* pi;  %[m2/m3]
        sd.Mass  = sd.M3 .* pi./6 .* sd.ro; %[ug m-3]
        
        sd.Dplot = [-9, -5];
        
        %Set Parameters for Merge Approaches
        sd.Active= [1  ,   0,     0,     0]; %Active Modes
        sd.dgHome= [1.e-7, 9.e-6, 6.e-6, 3.e-5]; %Home Base Diameters for DMM
        switch mergeType
            case 'KOR'
                sd.Threshold = [1.e-8, 1.e-7, 1.e-6, NaN]; %Thresholds for Merging
            case 'MMA'
            case 'DMM'
        end        
        
    case 'Three_Component_Base'
        sd.ro = 1400 .* 1e9; %Uniform Density [ug m-3]
        
        sd.nmode = 3;
        sd.smode = {'Ait','Acc','Cor'};    %Mode Names
        sd.dg    = [1.e-7, 5.e-7, 2.e-6]; %Mode Diameters [m]
        sd.sg    = [1.5,   1.5,   1.5];   %Mode Standard Deviation
        sd.Num   = [1000, 100,  10].*1.e6;  %Initial Number in Each Mode [N/m3]
        
        %Calculate Second and Third Moments as well as Surface Area and Mass Values
        sd.M1    = sd.Num .* sd.dg.^1 .* exp( 0.5  .* (log( sd.sg )) .^ 2 ); %[m/m3]
        sd.M2    = sd.Num .* sd.dg.^2 .* exp( 2    .* (log( sd.sg )) .^ 2 ); %[m2/m3]
        sd.M3    = sd.Num .* sd.dg.^3 .* exp( 9./2 .* (log( sd.sg )) .^ 2 ); %[m3/m3]
        sd.SA    = sd.M2 .* pi;  %[m2/m3]
        sd.Mass  = sd.M3 .* pi./6 .* sd.ro; %[ug m-3]
        
        sd.Dplot = [-9, -5];
        
        %Set Parameters for Merge Approaches
        sd.Active= [1,     1,      1    ]; %Active Modes
        sd.dgHome= [1.e-7, 5.e-7, 2.e-6]; %Home Base Diameters for DMM
        switch mergeType
            case 'KOR'
                sd.Threshold = [1.e-8, 1.e-7, NaN]; %Thresholds for Merging
            case 'MMA'
            case 'DMM'
        end
end


%For Sectional Approach, Project Size Distributions to User-Defined Sections
if strcmp(sd.aeroType,'MovingSect' ) || strcmp(sd.aeroType,'TOMAS' )
    sd.nbin = 200;
    DpLo    = 1e-9;  %m
    DpHi    = 1e-4;  %m
    sd.dp   = logspace( log10(DpLo),log10(DpHi),sd.nbin ); %m
    sd = project_size_dist( sd );   
end

%For Analytical Approach, Translate Modes Into Initial Condition
if strcmp( sd.aeroType,'Analytical' )
    init_mode = 1;
    sd.nbin = 200;
    sd.Num_alyt = sd.Num( init_mode );
    sd.sg_alyt  = sd.sg( init_mode );
    sd.dg_alyt  = sd.dg( init_mode );
end


%Set Minimum Parameters for each Aerosol Mode (or sections)
sd.Nmin  = 10 .* ones(1,sd.nmode); %Minimum Particle conc. (N/m3)
if strcmp( sd.aeroType,'Modal' )
    sd.sgHome= 1.5 .* ones(1,sd.nmode); %Home-Base Spread Parameter
    sd.M2min = sd.Nmin .* sd.dgHome.^2 .* exp( 2    .* (log( sd.sgHome )) .^ 2 ); %[m2/m3]
    sd.M3min = sd.Nmin .* sd.dgHome.^3 .* exp( 9./2 .* (log( sd.sgHome )) .^ 2 ); %[m3/m3]
elseif strcmp( sd.aeroType,'MovingSect' ) || strcmp(sd.aeroType,'TOMAS' )
    sd.Mmin = pi./6.*sd.dgHome.^3 .* sd.ro .* sd.Nmin; %[ug/m3]
end

end


%--------------------------------------------------------------------------
%
function sd = project_size_dist( sd )
%
%This function projects modal distribution initial size distribution
%parameters to a sectional representation
%--------------------------------------------------------------------------
%Allocate Number Array for Sectional Bins
sd.Num_sect = zeros(1,sd.nbin);
dp = sd.dp;

for ibin = 1:sd.nbin
    %Find the diameters bounding each bin
    if ibin == 1
        sd.dpHi(ibin) = sqrt(dp(1) .* dp(2));
        sd.dpLo(ibin) = dp(1).^2 ./ sd.dpHi(ibin);
    elseif ibin == sd.nbin
        sd.dpLo(ibin) = sqrt( dp(end-1) .* dp(end));
        sd.dpHi(ibin) = dp(end).^2 ./ sd.dpLo(ibin);
    else
        sd.dpLo(ibin) = sqrt( dp(ibin-1) .* dp(ibin));
        sd.dpHi(ibin) = sqrt( dp(ibin+1) .* dp(ibin));
    end
    
    %Find z-factor and apply error function
    zlo = log(sd.dpLo(ibin) ./ sd.dg) ./ (sqrt(2).*log(sd.sg)); %z-factors of all modes
    zhi = log(sd.dpHi(ibin) ./ sd.dg) ./ (sqrt(2).*log(sd.sg));
    sd.Num_sect(ibin) = sd.Num_sect(ibin) + sum(sd.Num./2 .* ( erf(zhi) - erf(zlo) )) ; %[N m-3]
    sd.Num_sect( sd.Num_sect == 0.0 ) = 1.0e-10;
end

sd.SA_sect = pi.*dp.^2    .* sd.Num_sect; %[m2 m-3]
sd.Mass_sect = pi./6.*dp.^3 .* sd.ro .* sd.Num_sect; %[ug m-3]

end
