function [ sdResult ] = evolve_analytical_dist( sd, vap, condrate, tout, dt, ...
    mergeType, Dp_Grow_Method )
%This is the time integration routine that simulates the evolution of the
%particle size distribution. It inputs a modal distirbution, converts it to
%an anlytical approach (Seinfeld, 1986) and integrates it in time

%Ben Murphy - Feb 2016

%Initialize Output Variables
tvec = dt:dt:tout;
nt   = length(tvec);

%Initialize sdResult Vector
sdResult.nbin  = [sd.nbin];
sdResult.Num_alyt = [sd.Num_alyt; zeros( nt,sd.nbin )];
sdResult.dgi_alyt  = [sd.dg_alyt; zeros( nt,sd.nbin )];
sdResult.SA_alyt  = [sd.SA_alyt; zeros( nt,sd.nbin )];
sdResult.Mass_alyt= [sd.Mass_alyt; zeros( nt,sd.nbin )];

%Loop Through Time Dimension
for it = 1:nt
    
    %Calculate Condensation Growth To Analytical Solution
    A = 0.009815; %um2 min-1
    At = A .* it .* dt./60; %um2
    
    %Project ANalytical Solution to Sectional Approximation
    for ibin = 1:sd.nbin
       sd.Mass_alyt = pi./6 .* sd.ro .*  
        
        
    end
    
    %[Later] Add Emissions Mode (or Advected Mode) after testing if one of
    %the existing modes will accomodate
        
         
    %Recalculate Mode Parameters
    sd = getpar(sd,limit_sg);
    
    %Save Variables in Output Structure
    sdResult.Num_sect(it+1,:)  = sd.Num_sect;
    sdResult.dg_sect(it+1,:)   = sd.dg_sect;
    sdResult.SA_sect(it+1,:)   = sd.SA_sect;
    sdResult.Mass_sect(it+1,:) = sd.Mass_sect;
    
end
end



