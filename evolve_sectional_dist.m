function [ sdResult ] = evolve_sectional_dist( sd, vap, condrate, tout, dt, ...
    mergeType, Dp_Grow_Method )
%This is the time integration routine that simulates the evolution of the
%particle size distribution. It inputs a modal distirbution, converts it to
%sectional and advances it in time.

%Ben Murphy - Feb 2016

%Initialize Output Variables
tvec = dt:dt:tout;
nt   = length(tvec);

%Initialize sdResult Vector
sdResult.nbin  = [sd.nbin];
sdResult.Num_sect = [sd.Num_sect; zeros( nt,sd.nbin )];
sdResult.dg_sect  = [sd.dg_sect; zeros( nt,sd.nbin )];
sdResult.SA_sect  = [sd.SA_sect; zeros( nt,sd.nbin )];
sdResult.Mass_sect= [sd.Mass_sect; zeros( nt,sd.nbin )];

%Loop Through Time Dimension
for it = 1:nt
    
    %Calculate Condensation Growth To each Section (TOMAS)
    sd = tomas( sd, condrate, dt);
       
    %[Later] Add Emissions Mode (or Advected Mode) after testing if one of
    %the existing modes will accomodate
        
         
    %Recalculate Mode Parameters
    sd = getpar(sd,limit_sg);
    
    %Save Variables in Output Structure
    sdResult.Num_sect(it+1,:)  = sd.Num_sect;
    sdResult.dg_sect(it+1,:)   = sd.dg_sect;
    sdResult.SA_sect(it+1,:)   = sd.SA_sect;
    sdResult.Mass_sect(it+1,:) = sd.Mass_sect;
    
end
end


