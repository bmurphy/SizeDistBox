function dNdlog10dp = get_ndist_sect( xdg, sd, it )

%Calculate the size distribution using all bin boundaries at a particular
%time.
DpLo = zeros(1,length(xdg));
DpHi = zeros(1,length(xdg));
DpLo(2:end) = sqrt( xdg(1:end-1) .* xdg(2:end) );
DpLo(1) = xdg(1).^2 ./ DpLo(2);
DpLo( isnan(DpLo) ) = 0.0;
DpHi(1:end-1) = DpLo(2:end);
DpHi(end) = xdg(end).^2 ./ DpHi(end-1);


%Calculate dNdlog10dp Vector   
dNdlog10dp = sd.Num_sect(it,:) ./ log10(DpHi./DpLo);
dNdlog10dp( isnan(dNdlog10dp) ) = 0.0;

end