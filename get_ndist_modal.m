function dNdlog10dp = get_ndist_modal( xdg, sd, it )

%Calculate the size distribution using all modes at a particular time it.
pi = 3.1415;
dNdlog10dp = zeros( 1+sd.nmode,length(xdg) );

for idg = 1:length(xdg)
    
  dNtmp = [2.303 .* sd.Num(it,:) ./ ( sqrt(2*pi) .* log(sd.sg(it,:)) ) ...
                   .* exp( -(log( xdg(idg) ) - log(sd.dg(it,:)) ).^2 ./ ...
                             (2.*(log(sd.sg(it,:))).^2) )]';
                         
  dNdlog10dp(:,idg) = [sum(dNtmp); dNtmp];
                         
  
         
end

end