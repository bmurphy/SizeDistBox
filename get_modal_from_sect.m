function [Num, gmd, stdev] = get_modal_from_sect( sd )

%Calculate the size distribution using all modes at a particular time it.
pi = 3.1415;
Num = zeros(1,size(sd.dp,1));
gmd = zeros(1,size(sd.dp,1));
stdev = zeros(1,size(sd.dp,1));

for it = 1:size(sd.dp,1)
    
    %Get Diameter Vecotor of Distribution
    Dp = sd.dp(it,:);
    A = log(Dp);
    A( isinf(A) ) = -30;
    
    %Get Number (N m-3)
    Num(it) = sum( sd.Num_sect(it,:) );
    
    %Get Geometric Mean Diameter - calculate this as the exponential of the
    %weighted-average of natural log of particle diameters
    gmd(it) = exp( sum( sd.Num_sect(it,:)./Num(it) .* A ));
    
    %Get Standard Deviation
    stdev(it)  = exp( std ( sd.Num_sect(it,:)./Num(it) .* A ) ./ sqrt(2) );

end

end