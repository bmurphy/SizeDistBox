function [sd, Dcut] = DMM( sd, GM0, GM2, GM3, dt )

%Dynamic Mode Manager from Whitby et al (2005)
%Coded in MATLAB by Ben Murphy September 2015
gamma = 0.1;       %Mode Proximity Criterion
                   %Gamma is the number of standard deviations within which
                   %mode i must come to mode j for reallocation to happen.

delta_crit = 0.01;  %Mode Comparison Criterion 
                   %Larger delta_crit is more likely to lead to
                   %reallocaiton


pi = 3.1415;
Dcut = zeros( 2,sd.nmode,3 ) .* NaN;
sd.Jij = zeros( sd.nmode,6 ) .* NaN;
sd.Jij_dest = zeros( 1,sd.nmode ) .* NaN;

for imode = 1:sd.nmode
    if sd.Active(imode)
    %%%%%%%%% GROWING AND SHRINKING MODES    %%%%%%%%%%
    %Find the next highest mode
    lgrow = 0;  %Mode to grow into
    dgrow = 1e-3; %Diameter of next highest mode [m]
    for jmode = 1:sd.nmode
        if (imode ~= jmode && sd.dgHome(jmode) > sd.dgHome(imode) && sd.dgHome(jmode) < dgrow)
            dgrow = sd.dgHome(jmode);
            
            %%% Test for Transfer Criteria
            %Mode Proximity Criterion (Eqn 8)
            %Are the mode centers close enough so that you don't merge with
            %something not even nearby
            x = sd.dg(imode) .* (sd.sg(imode) .^ gamma) ./ sd.dg(jmode); %Eqn 8a
            y = sd.dg(imode) .* (sd.sg(jmode) .^ gamma) ./ sd.dg(jmode); %Eqn 8b
            if ( x > 1.0 && y > 1.0 )
            
              %Mode-Comparison Criterion (Eqn 9)
              %Is the hypothetical state, where the modes are fully merged,
              %close enough that they already look kind of alike.              
              k1 = 2;
              k2 = 3;
              r = k1/k2;
              khat1 = 1/[r .* (k2 - k1)];
              khat2 = r./(k1-k2);
              
              Numc = sd.Num(imode) + sd.Num(jmode);             %Eqn B.1a
              Mk1barc = (sd.M2(imode) + sd.M2(jmode)) / Numc;   %Eqn B.1b
              Mk2barc = (sd.M3(imode) + sd.M3(jmode)) / Numc;   %Eqn B.1c
              
              
              dgc = Mk1barc ^ khat1 * Mk2barc ^ khat2;  %Eqn A.5a
              sgc = exp( (2/(k1*(k1-k2)) * log(Mk1barc/ (Mk2barc ^ r)) )^0.5 ); %Eqn A.5b
              
              sghatij = log( sd.sg(imode) ) / log( sd.sg(jmode) );  %Eqn B.4
              sghatic = log( sd.sg(imode) ) / log( sgc );           %Eqn B.4
              sghatjc = log( sd.sg(jmode) ) / log( sgc );           %Eqn B.4
              
              dhatij  = log( sd.dg(imode) / sd.dg(jmode) ) / log(sd.sg(jmode)); %Eqn B.4
              dhatic  = log( sd.dg(imode) / dgc ) / log( sgc ); %Eqn B.4
              dhatjc  = log( sd.dg(jmode) / dgc ) / log( sgc ); %Eqn B.4
              
              bigDelta_T1 = (Numc/sd.Num(imode))^2 / log(sgc);
              
              bigDelta_T2 = 1/log(sd.sg(imode));
              
              bigDelta_T3 = (sd.Num(jmode)/sd.Num(imode))^2 / log(sd.sg(jmode));
              
              bigDelta_T4 = 2 .* sd.Num(jmode)/sd.Num(imode)/ (sqrt(0.5*(1+sghatij^2)) * log(sd.sg(jmode)) ) ...
                            * exp( -0.5 * dhatij^2 / (1+sghatij^2));
                        
              bigDelta_T5 = 2 * Numc/sd.Num(imode) / (sqrt(0.5*(1+sghatic^2)) * log(sgc)) ...
                              * exp(-0.5 * dhatic^2/(1+sghatic^2));
                          
              bigDelta_T6 = 2 * sd.Num(jmode)*Numc/sd.Num(imode)^2 / (sqrt(0.5*(1+sghatjc^2)) * log(sgc)) ...
                              * exp(-0.5 * dhatjc^2/(1+sghatjc^2));
                          
              bigDelta = bigDelta_T1 + bigDelta_T2 + bigDelta_T3 + bigDelta_T4 - bigDelta_T5 - bigDelta_T6;
              
              numer = bigDelta;
              
              denom = 1/log(sd.sg(imode)) ...
                      + sd.Num(jmode)/sd.Num(imode)/ (sqrt(0.5*(1+sghatij^2)) * log(sd.sg(jmode)) ) ...
                         * exp( -0.5 * dhatij^2 / (1+sghatij^2)) ...
                      + (sd.Num(jmode)/sd.Num(imode))^2 / log(sd.sg(jmode));
              
              delta = abs(numer) / denom;
              
              if ( delta < delta_crit )
                lgrow = jmode;
              end
                
            end
        end
    end
    
    if (lgrow)
        jmode = lgrow;
        sd.Active(jmode) = 1;
        
        %Subscript Definitions
        %imode - index of donor mode (index i)
        %jmode - index of receptor mode (index j)
        
        %Calculate Moment Flux Across Boundary
        k = 3;    %Keyed Moment
        kv = 3;   %Vector index of keyed moment
        
        Mi(1) = sd.Num(imode);
        Mi(2) = sd.M2(imode);
        Mi(3) = sd.M3(imode);
        Mj(1) = sd.Num(jmode);
        Mj(2) = sd.M2(jmode);
        Mj(3) = sd.M3(jmode);
        
        dMidt(1) = GM0(imode);
        dMjdt(1) = GM0(jmode);
        dMidt(2) = GM2(imode);
        dMjdt(2) = GM2(jmode);
        dMidt(3) = GM3(imode);
        dMjdt(3) = GM3(jmode);
        
        d0i = sd.dg(imode);
        d0j = sd.dg(jmode);
        dki = sd.dg(imode) .* exp(k .* log(sd.sg(imode)).^2);  %Eqn A.2
        dkj = sd.dg(jmode) .* exp(k .* log(sd.sg(jmode)).^2);  %Eqn A.2
       
        %%% Step 1: Calculate the Basic Flux Across the Boundary
        %Calculate the Virtual Boundary (Eqn 1)
        %db = sqrt( dki .* dkj );
        db = sqrt( d0i .* d0j );
        
        xi = log( db./dki ) ./ log( sd.sg(imode) ); %Eqn 3: 3rd line
        xj = log( db./dkj ) ./ log( sd.sg(jmode) ); %Eqn 3: 4th line
        
        dNumi_dt = GM0(imode);  %No number Change Other than Merging
        dNumj_dt = GM0(jmode);  %No number Change Other than Merging
        
        %The next chunk is taken from Eqn A.5a
        k1 = 2;
        k2 = 3;
        r = k1/k2;
        khat1 = 1/(r .* (k2 - k1));
        khat2 = r./(k1-k2);
        
        Mk1bari = sd.M2(imode) ./ sd.Num(imode); %Eqn A.5a
        Mk2bari = sd.M3(imode) ./ sd.Num(imode);
        d0i = (Mk1bari .^ khat1) .* (Mk2bari .^ khat2);
        sigmai = exp( sqrt( 2/(k1 .* (k1-k2) ) .* log(Mk1bari./ (Mk2bari.^r)) ));
        
        Mk1barj = sd.M2(jmode) ./ sd.Num(jmode); %Eqn A.5a
        Mk2barj = sd.M3(jmode) ./ sd.Num(jmode);
        d0j = (Mk1barj .^ khat1) .* (Mk2barj .^ khat2);
        sigmaj = exp( sqrt( 2/(k1 .* (k1-k2) ) .* log(Mk1barj./ (Mk2barj.^r)) ));
           
        %dx_dt Terms are calculated with Eqn A.6
        dxi_dt_T1 = khat1/Mj(k1)*dMjdt(k1) + khat2/Mj(k2)*dMjdt(k2) - (khat1+khat2)/sd.Num(jmode)*dNumj_dt;
        dxi_dt_T2 = khat1/Mi(k1)*dMidt(k1) + khat2/Mi(k2)*dMidt(k2) - (khat1+khat2)/sd.Num(imode)*dNumi_dt;
        dxi_dt_T3 = 2*(xi+2*k*log(sd.sg(imode))) ./ (k1*(k1-k2)*log(sd.sg(imode))) ...
                    .* (1/Mi(k1)*dMidt(k1) - r/Mi(k2)*dMidt(k2) - (1-r)/sd.Num(imode)*dNumi_dt);
                
        dxi_dt = 1/(2*log(sd.sg(imode))) * ( dxi_dt_T1 - dxi_dt_T2 - dxi_dt_T3 );
        
        dxj_dt_T1 = dxi_dt_T2;
        dxj_dt_T2 = dxi_dt_T1;
        dxj_dt_T3 = 2*(xj+2*k*log(sd.sg(jmode))) ./ (k1*(k1-k2)*log(sd.sg(jmode))) ...
                    .* (1/Mj(k1)*dMjdt(k1) - r/Mj(k2)*dMjdt(k2) - (1-r)/sd.Num(jmode)*dNumj_dt);
        dxj_dt = 1/(2*log(sd.sg(jmode))) * ( dxj_dt_T1 - dxj_dt_T2 - dxj_dt_T3 );
        
        %The fluxes are calculated with Eqn 4
        Jij = 0.5 .* erfc( xi/sqrt(2) ) .* dMidt(kv) - ...
                 Mi(kv)/sqrt(2.*pi) .* exp(-0.5.* xi.^2) .* dxi_dt;
        Jji = 0.5 .* ( 1 + erf( xj/sqrt(2) ) ) .* dMjdt(kv) + ...
                 Mj(kv)/sqrt(2.*pi) .* exp(-0.5.* xj.^2) .* dxj_dt;
             
        %%% Step 2: Modification for Degree of Donor Behavior
        %Calculate distance from home-base (Eqn 5)
        Dij = abs( log10( sd.dg(imode) / sd.dgHome(imode) ) ./ ...
                   log10( sd.dgHome(imode) / sd.dgHome(jmode) ) );
        Dji = abs( log10( sd.dg(jmode) / sd.dgHome(jmode) ) ./ ...
                   log10( sd.dgHome(imode) / sd.dgHome(jmode) ) );
               
        %%% Step 3: Factor to ensure Complete Moment Transfer
        %Calculate R term (Eqn 7c)
        epsn  = 1.e-3;
        intgi = 0.5-0.5*erf( log( db/d0i ) / (sqrt(2)*log(sd.sg(imode))));
        intgj =  0.5+0.5*erf( log( db/d0j ) / (sqrt(2)*log(sd.sg(jmode))));
        
        Ri = (1 + epsn) ./ (1 - intgi + epsn );
        Rj = (1 + epsn) ./ (1 - intgj + epsn );
        
        %%% Put it all together!
        JijM = zeros(1,3);
        JjiM = zeros(1,3);
        JijM(kv) = Jij .* Dij .* Ri;  %Transfer for the keyed Moment
        JjiM(kv) = Jji .* Dji .* Rj;  %Transfer for the keyed Moment
        
        %Calculate the Modal Flux of All the Separate Moments 
        imfill = 1:3;
        for im = imfill(imfill ~= kv)
            JijM(im) = JijM(kv) ./ Mi(kv) .* Mi(im);
            JjiM(im) = JjiM(kv) ./ Mj(kv) .* Mj(im);
        end
       
        %%% Updates Moments of Both Modes (Make this into a 2D array that
        %%% is summed simultaneously for all modes after all of the
        %%% transfers are calculated)
        sd.Jij_dest(imode) = jmode;
        sd.Jij(imode,1) = JijM(1); % 0 Moment from imode to jmode
        sd.Jij(imode,2) = JjiM(1); % 0 Moment from jmode to imode
        sd.Jij(imode,3) = JijM(2); % 2 Moment from imode to jmode
        sd.Jij(imode,4) = JjiM(2); % 2 Moment from jmode to imode
        sd.Jij(imode,5) = JijM(3); % 3 Moment from imode to jmode
        sd.Jij(imode,6) = JjiM(3); % 3 Moment from jmode to imode
    end
    end
    
end

%Simultaneously Update All of the Modes wrt Merging Fluxes and Condensation
%Fluxes
%Try only growing the smallest mode
%g = find( sd.Active == 1, 1 );
%sd.Num(g) = sd.Num(g) + GM0(g) .* dt; %.* sd.Active;
%sd.M2(g)  = sd.M2(g)  + GM2(g) .* dt; %.* sd.Active;
%sd.M3(g)  = sd.M3(g)  + GM3(g) .* dt; %.* sd.Active;

sd.Num = sd.Num + GM0 .* dt.* sd.Active;
sd.M2  = sd.M2  + GM2 .* dt.* sd.Active;
sd.M3  = sd.M3  + GM3 .* dt.* sd.Active;

for imode = 1:sd.nmode
    jmode = sd.Jij_dest( imode );  %Index of Counterpart Mode
    
    if (jmode == jmode)
        sd.Num(imode) = sd.Num(imode) + dt.*( sd.Jij(imode,2) - sd.Jij(imode,1) );
        sd.Num(jmode) = sd.Num(jmode) + dt.*( sd.Jij(imode,1) - sd.Jij(imode,2) );
        sd.M2(imode)  = sd.M2(imode)  + dt.*( sd.Jij(imode,4) - sd.Jij(imode,3) );
        sd.M2(jmode)  = sd.M2(jmode)  + dt.*( sd.Jij(imode,3) - sd.Jij(imode,4) );
        sd.M3(imode)  = sd.M3(imode)  + dt.*( sd.Jij(imode,6) - sd.Jij(imode,5) );
        sd.M3(jmode)  = sd.M3(jmode)  + dt.*( sd.Jij(imode,5) - sd.Jij(imode,6) );
        
        %Deactivate and reset modes if the Number moment Drops below the
        %minimum threshold Nmin
        if sd.Num(imode) < sd.Nmin(imode)
            sd.Active(imode) = 0;
            
            sd.Num(jmode) = sd.Num(jmode) + sd.Nmin(imode)  - sd.Num(imode);
            sd.M2(jmode)  = sd.M2(jmode)  + sd.M2min(imode) - sd.M2(imode);
            sd.M3(jmode)  = sd.M3(jmode)  + sd.M3min(imode) - sd.M3(imode);
            
            sd.Num(imode) = sd.Nmin(imode);
            sd.M2(imode)  = sd.M2min(imode);
            sd.M3(imode)  = sd.M3min(imode);
            
        elseif sd.Num(jmode) < sd.Nmin(jmode)
            sd.Active(jmode) = 0;
            
            sd.Num(imode) = sd.Num(imode) + sd.Nmin(jmode)  - sd.Num(jmode);
            sd.M2(imode)  = sd.M2(imode)  + sd.M2min(jmode) - sd.M2(jmode);
            sd.M3(imode)  = sd.M3(imode)  + sd.M3min(jmode) - sd.M3(jmode);
            
            sd.Num(jmode) = sd.Nmin(jmode);
            sd.M2(jmode)  = sd.M2min(jmode);
            sd.M3(jmode)  = sd.M3min(jmode);            
        end
        
    end
    
end


end
    
    
    
    
    
    
    
    
