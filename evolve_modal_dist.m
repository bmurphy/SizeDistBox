function [ sdResult ] = evolve_modal_dist( sd, vap, condrate, tout, dt, ...
    mergeType, M3_Grow_Method, M2_Grow_Method )
%This is the time integration routine that simulates the evolution of the
%particle size distribution. It takes any number of modes with any
%user-defined parameters.

%The model applies the changes via Euler steps since that how CMAQ operates
%now. The Mode Merging Algorithm and Dynamic Mode Manager are Modules to be
%called. Condensation is just a user-defined input, although the fraction
%going to each mode is calculated online. Coagulation is ignored.
%Evaporation should also work just by making the condrate negative

%Ben Murphy - Sept 2015

if strcmp(mergeType,'DMM'); limit_sg = 0; else limit_sg = 1; end
%limit_sg = 1;

%Initialize Output Variables
tvec = dt:dt:tout;
nt   = length(tvec);

%Initialize sdResult Vector
sdResult.nmode = [sd.nmode];
sdResult.Num   = [sd.Num; zeros( nt,sd.nmode )];
sdResult.dg    = [sd.dg; zeros( nt,sd.nmode )];
sdResult.sg    = [sd.sg; zeros( nt,sd.nmode )];
sdResult.M1    = [sd.M1; zeros( nt,sd.nmode )];
sdResult.M2    = [sd.M2; zeros( nt,sd.nmode )];
sdResult.M3    = [sd.M3; zeros( nt,sd.nmode )];
sdResult.M4    = [sd.M4; zeros( nt,sd.nmode )];
sdResult.M6    = [sd.M6; zeros( nt,sd.nmode )];
sdResult.SA    = [sd.SA; zeros( nt,sd.nmode )];
sdResult.Mass  = [sd.Mass; zeros( nt,sd.nmode )];
sdResult.Active= [sd.Active; zeros( nt,sd.nmode )];
sdResult.GM6   = [zeros( nt+1,sd.nmode )];
sdResult.GM4   = [zeros( nt+1,sd.nmode )];
sdResult.GM3   = [zeros( nt+1,sd.nmode )];
sdResult.GM2   = [zeros( nt+1,sd.nmode )];
sdResult.GM1   = [zeros( nt+1,sd.nmode )];
sdResult.Jij   = zeros( nt,sd.nmode,6 );

%Meteorology Variables are Standard Here
met.T = 298;    %Kelvin
met.P = 101325; %Pascals


%Loop Through Time Dimension
for it = 1:nt

    %[Later] Add Emissions Mode (or Advected Mode) after testing if one of
    %the existing modes will accomodate
    
    GM1 = 0;
    GM2 = 0;
    GM3 = 0;
    GM4 = 0;
    GM6 = 0;
    
    switch mergeType
        case {'None','KOR', 'MMA'}
            %Calculate Fraction of Condensation to Each Mode
            sd.Active= ones(size(sd.Active)); %Active Modes
            [Ifac3, omega3]  = get_growth_fac3(sd, vap);   %Size-dep I growth factor from 
                                                %Eqn A7; Binkowski and Shankar (1995)
            [Ifac4, omega4]  = get_growth_fac4(sd, vap, met);   %Size-dep I growth factor from
                                                %Nieminen et al
            [Ifac6, omega6]  = get_growth_fac6(sd, vap, met);   %Size-dep I growth factor from
                                                %Nieminen et al
            [IfacN, omegaN]  = get_integ_growth_fac4(sd, vap, met);   %Size-dep I growth factor from
                                                %Nieminen et al
               
            Ifac = IfacN;
            omega = omegaN;
                                                
            %Add Condensation (Eqn 7a,b; Binkowski and Shankar, 1995)
            GM0    = zeros(1,size(omega,2));                                                                                    
            switch M3_Grow_Method
                case 'Conc'
                    dCvap = max( vap.conc - vap.Cstar );
                    M3rate = 6./pi .* dCvap ./ sd.ro .* sum(Ifac(3,:)); %[m3 m-3 s-1]
                otherwise     
                    M3rate = condrate ./ sd.ro ./ (pi./6); %[m3 m-3 s-1]
            end
            if M3rate > 0
                M3rate = min( M3rate,vap.conc./dt./sd.ro./(pi./6) );
            end
            GM3    = M3rate .* omega(3,:);
%             if M3rate < 0
%                 GM3(:) = max( GM3(:), -sd.Mass(:)./dt./sd.ro./(pi./6) );
%             end
            
            switch M2_Grow_Method
                case 'Evolve'
                    GM2    = GM3 .* (2/3) .* Ifac(2,:) ./ Ifac(3,:);
                case 'M1'
                    GM1    = sign(GM3) .* ...
                        abs( GM3 .* (1/3) .* Ifac(1,:) ./ Ifac(3,:) ) .^(1/1);
                case 'M4'
                    GM4    = GM3 .* (4/3) .* Ifac(4,:) ./ Ifac(3,:);
                case 'M6'
                    GM6    = GM3 .* (6/3) .* Ifac(5,:) ./ Ifac(3,:);
                case 'skew'
                    GM1    = sign(GM3) .* ...
                        abs( GM3 .* (1/3) .* Ifac(1,:) ./ Ifac(3,:) ) .^(1/1);
                    GM2    = GM3 .* (2/3) .* Ifac(2,:) ./ Ifac(3,:);
                case 'Const_Stdev'
                    M3_new = sd.M3 + GM3 .* dt;
                    M2_new = sd.M2 .* ( M3_new ./ sd.M3 ).^(2./3.);
                    GM2 = (M2_new - sd.M2) ./ dt; %[m2/m3]   
            end            
                        
        case 'DMM'
            %Change in Diameter
            switch M3_Grow_Method
                case 'Conc'
                    %Make modes grow in response to a concentration of
                    %vapor
                    sd.Active= ones(size(sd.Active)); %Active Modes
                    [Ifac, omega]  = get_growth_fac(sd, vap);   %Size-dep I growth factor from
                    %Eqn A7; Binkowski and Shankar (1995)
                    
                    %Add Condensation (Eqn 7a,b; Binkowski and Shankar, 1995)
                    GM0    = zeros(1,size(omega,2));
                    M3rate = conc .* sum(Ifac) ./ sd.ro ./ (pi./6); %[m3 m-3 s-1]
                    GM3    = M3rate .* omega(2,:);
                    
                    switch M2_Grow_Method
                        case 'Evolve'
                            GM2    = GM3 .* (2/3) .* Ifac(1,:) ./ Ifac(2,:);
                        case 'Const_Stdev'
                            M3_new = sd.M3 + GM3 .* dt;
                            M2_new = sd.M2 .* ( M3_new ./ sd.M3 ).^(2./3.);
                            GM2 = (M2_new - sd.M2) ./ dt; %[m2/m3]
                    end
                    
                    
                case 'Log_Growth'
                    dlnDpdt = 20; %Change in diameters [ lnDp s-1]
                    dg_new = exp( log(sd.dg) + dlnDpdt .* dt ); %.* sd.Active );
                    %Calculate Change in Each Mode
                    GM0 = zeros(1,sd.nmode );
                    GM2 = (sd.Num .* dg_new.^2 .* exp( 2    .* (log( sd.sg )) .^ 2 ) - sd.M2) ./ dt; %[m2/m3]
                    GM3 = (sd.Num .* dg_new.^3 .* exp( 9./2 .* (log( sd.sg )) .^ 2 ) - sd.M3) ./ dt; %[m3/m3]
                    
                case 'Linear_Growth'
%                     dDpdt = (condrate./sd.ro./(pi./6)).^(1/3);
                    dDpdt = 1.0e-4;  % m s-1 (Condensation Velocity)
                    dg_new = sd.dg + dDpdt .* dt;      
                    %Calculate Change in Each Mode while keeping Constant
                    %sigma
                    GM0 = zeros(1,sd.nmode );
                    GM2 = (sd.Num .* dg_new.^2 .* exp( 2    .* (log( sd.sg )) .^ 2 ) - sd.M2) ./ dt; %[m2/m3]
                    GM3 = (sd.Num .* dg_new.^3 .* exp( 9./2 .* (log( sd.sg )) .^ 2 ) - sd.M3) ./ dt; %[m3/m3]
                    
                case 'Mass_Growth'
                    %Calculate Fraction of Condensation to Each Mode
                    sd.Active= ones(size(sd.Active)); %Active Modes
                    [Ifac, omega]  = get_growth_fac(sd, vap);   %Size-dep I growth factor from
                    %Eqn A7; Binkowski and Shankar (1995)
                    
                    %Add Condensation (Eqn 7a,b; Binkowski and Shankar, 1995)
                    GM0    = zeros(1,size(omega,2));
                    M3rate = condrate ./ sd.ro ./ (pi./6); %[m3 / m3]
                    GM3    = M3rate .* omega(2,:);
                    switch M2_Grow_Method
                        case 'Evolve'
                            GM2    = GM3 .* (2/3) .* Ifac(1,:) ./ Ifac(2,:);
                        case 'Const_Stdev'
                            M3_new = sd.M3 + GM3 .* dt;
                            M2_new = sd.M2 .* ( M3_new ./ sd.M3 ).^(2./3.);
                            GM2 = (M2_new - sd.M2) ./ dt; %[m2/m3]
                    end
            end            
    end

    %Apply Mode Merging
    switch mergeType
        case 'None'
            Dcut = zeros( 2,sd.nmode,3 ) .* NaN;
            %Update 2nd and 3rd Moments with Condensation
            ind = sd.Mass > 1.0e-6;
            sd.M3( ind ) = sd.M3( ind ) + GM3( ind ) .* dt;
            
            if strcmp( M2_Grow_Method, 'M1' )
                sd.M1( ind ) = sd.M1( ind ) + GM1( ind ) .* dt;
            elseif strcmp( M2_Grow_Method, 'M4' )
                sd.M4( ind ) = sd.M4( ind ) + GM4( ind ) .* dt;
            elseif strcmp( M2_Grow_Method, 'M6' )
                sd.M6( ind ) = sd.M6( ind ) + GM6( ind ) .* dt;
            elseif strcmp( M2_Grow_Method, 'skew' )
                sd.M1( ind ) = sd.M1( ind ) + GM1( ind ) .* dt;
                sd.M2( ind ) = sd.M2( ind ) + GM2( ind ) .* dt;
            else
                sd.M2( ind ) = sd.M2( ind ) + GM2( ind ) .* dt;
            end
   
        case 'KOR'
            %Update 2nd and 3rd Moments with Condensation
            sd.M3 = sd.M3 + GM3 .* dt;
            sd.M2 = sd.M2 + GM2 .* dt;
    
            %Recalculate Mode Parameters
            sd = getpar(sd,limit_sg);
            
            %Do Mode Merging
            [sd, Dcut] = Korhola(sd, GM3); 

        case 'MMA'
            sd.M3old = sd.M3;
            sd.M2old = sd.M2;
            %Update 2nd and 3rd Moments with Condensation
            sd.M3 = sd.M3 + GM3 .* dt;
            sd.M2 = sd.M2 + GM2 .* dt;
            %sd.M2 = sd.M2old .* (sd.M3 ./ sd.M3old).^(2/3);
            %GM2   = (sd.M2 - sd.M2old) ./ dt; 
             
            %Recalculate Mode Parameters
            sd = getpar(sd,limit_sg);
            
            %Do Mode Merging
            [sd, Dcut] = MMA(sd, GM3); 

        case 'DMM'
            %Dynamic Mode Merging Algorithm (Whitby et al., 2005)
%             fprintf(1,'Before DMM: %5.1d %5.1d %5.1d %5.1d\n',sd.sg)
            [sd, Dcut] = DMM(sd, GM0, GM2, GM3, dt); 
            
            sdResult.Jij(it+1,:,:) = shiftdim(sd.Jij(:,:),-1);
    end
    
    %Recalculate Mode Parameters
%     fprintf(1,'Before getpar: %5.1d %5.1d %5.1d %5.1d\n',sd.sg)
    if strcmp( M2_Grow_Method, 'skew' )
        sd = getpar_skew(sd,limit_sg);
    else
        sd = getpar(sd,limit_sg);
    end
    
    %Update Vapor concentration, if necessary
    if strcmp( M3_Grow_Method,'Conc' )
        vap.conc = vap.conc -(sum(sd.Mass) - sum(sdResult.Mass(it,:),2) );
    end
    
    %Save Variables in Output Structure
    sdResult.Num(it+1,:)  = sd.Num;
    sdResult.dg(it+1,:)   = sd.dg;
    sdResult.sg(it+1,:)   = sd.sg;
    sdResult.M1(it+1,:)   = sd.M1;
    sdResult.M2(it+1,:)   = sd.M2;
    sdResult.M3(it+1,:)   = sd.M3;
    sdResult.M4(it+1,:)   = sd.M4;
    sdResult.M6(it+1,:)   = sd.M6;
    sdResult.SA(it+1,:)   = sd.SA;
    sdResult.Mass(it+1,:) = sd.Mass;
    sdResult.Active(it+1,:)= sd.Active;
    sdResult.GM6(it+1,:)  = GM6;
    sdResult.GM4(it+1,:)  = GM4;
    sdResult.GM3(it+1,:)  = GM3;
    sdResult.GM2(it+1,:)  = GM2;
    sdResult.GM1(it+1,:)  = GM1;
    sdResult.Dcut_grow(it+1,:,:) = Dcut(1,:,:);
    sdResult.Dcut_shrink(it+1,:,:) = Dcut(2,:,:);

end
end


